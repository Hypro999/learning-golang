- Classification of data types in Go:
    1. Basic data types (uint8, int, float, float64, string, bool, etc.)
    2. Aggregate data types (arrays and structs)
    3. Reference data types (pointers, slices, maps, channels, functions)
    4. Interfaces

- When using ints as bitsets, always use unsigned integers to make sure that
  the sign bit is not used instead of using 0 during (right) shift operations.
  "Left shifts fill the vacated bits with zeros, as do right shifts of unsigned
  numbers, but right shifts of signed numbers fill the vacated bits with copies
  of the sign bit."

- Stick to float64 as much as possible.

- +Inf, -Inf, and NaN are considered as part of the IEEE floating point standard
  and are supported in this language (e.g. `math.NaN()`). `0/0` is still illegal
  but something like:
```go
z := 0.0
fmt.Printf("%f\n", 1/z)
```
  is legal and will yeild "+Inf".

- The Go http server (`net/http`) "uses standard heuristics to recognize common
  formats like PNG from the first 512 bytes of the response (magic bytes) and
  generates the proper (content type) header.". This is actually kind of cool.

- "The built-in len function returns the number of bytes (not runes) in a string"
  This is a very important point because in Go, like in Python, strings are
  interpreted as utf-8 encoded unicode strings. In utf-8 each character can be
  anywhere from 1-4 bytes long. To get the character length of each string we'd have
  to use: 

- "The index operation s[i] retrieves the i-th byte of string s" (similar to the
  point above - strings are basically byte arrays).

- Strings in Go are immutable (just like in Python). So string operations will create
  a new string. This can be expensive under certain conditions. So use some kind of
  string builder (`strings.Builder`) then convert to a string at the end for lengthy string
  manipulation operations. `fmt.Sprintf` is also really useful (similar to `sprintf` from C
  and `str.format` in Python).

- "Since strings are immutable, constructions that try to modify a string’s data in place
  are not allowed:
  s[0] = 'L' // compile error: cannot assign to s[0]"

- Go source files are always supposed to be encoded in utf-8.

- `\xhh` can be used to inject raw bytes into string literals.

- *Raw String Literals*: A way to achieve multiline strings. No escape sequences will be
  processed. Use \`...\` for raw string literals. They are also good for avoiding "backspace
  hell" when writing regex strings.

- UTF-8 encoding for Unicode:
```
0xxxxxx                                 runes 0−127         (ASCII)
11xxxxx 10xxxxxx                        128−2047            (values <128 unused)
110xxxx 10xxxxxx 10xxxxxx               2048−65535          (values <2048 unused)
1110xxx 10xxxxxx 10xxxxxx 10xxxxxx      65536−0x10ffff      (other values unused)
```

- "The unicode package provides functions for working with individual runes (such as
   distinguishing letters from numbers, or converting an upper-case letter to a lower-case
   one), and the unicode/utf8 package provides functions for encoding and decoding runes as
   bytes using UTF-8."

- Unicode escapes in Go: `\uhhhh` for 16-bit codes and `\Uhhhhhhhh` for 32-bit codes where
  `h` is a hexadecimal digit.

- Go’s range loop, when applied to a string, performs UTF-8 decoding implicitly.

- Main standard library packages for manipulating strings: `bytes`, `strings`, `strconv`, `unicode`.

- `IsDigit`, `IsLetter`, `IsUpper`, `IsLower` are all part of the `unicode` package.

- The `strings` package provides `ToUpper` and `ToLower`.

- `fmt.Sprintf` and `strconv.Itoa` are both ways to convert an integer to a string
   though I think the former is used more commonly (because that's the C way of doing
   things). Use `strconv.Atoi` or `strconv.ParseInt` for the reverse conversion.

- Similar to C, Go has `fmt.Scanf`.

- Quirk for defining constants:
```go
const (
    a = 1
    b
    c = 2
    d
)

fmt.Println(a, b, c, d) // "1 1 2 2"
```

- Enums are supported in Go via. the `iota` keyword, a.k.a. the "Constant Generator".
  (See the example file for examples).

- If you don't specify an explicit type for a Go constant then it becomes an "untyped"
  or "uncommitted" constant. There are 6 fundamental uncommitted constants:
    1. untyped boolean
    2. untyped integer
    3. untyped rune
    4. untyped float
    5. untyped complex
    6. untyped string
  These uncommitted types are a represented in the compiler with a high precision of
  atleast 256 bits (32 bytes!). Only constants can be untyped/uncommitted.

  - Untyped integers by default translate to `int` (if using interface{}?) but
  untyped complex and untyped float are by default converted to `float64` and
  `complex128` by default.