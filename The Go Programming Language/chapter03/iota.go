package chapter03

// Sample code from the textbook for how to use iota, the contant generator.

type Weekday int

const (
	Sunday Weekday = iota // Sunday will be 0, just like with enums
	Monday
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
)

type Flag uint

// Iota can also be used in compile-time determinable expressions. So here, we
// can create bitmasks like 00001, 00010, 00100, 01000, 10000. Pretty cool!
// Reminder: to check if a bit is set, do something like `x & FlagUp == FlagUp`
const (
	FlagUp           Flag = 1 << iota // is up
	FlagBroadcast                     // supports broadcast access capability
	FlagLoopback                      // is a loopback interface
	FlagPointToPoint                  // belongs to a point-to-point link
	FlagMulticast                     // supports multicast access capability
)

// The following functions are modified versions from the ones in the book.
// Here we use receiver functions (methods) on the new type since that makes more sense.

func (v Flag) IsUp() bool {
	return v&FlagUp == FlagUp
}

func (v *Flag) SetUp() {
	*v |= FlagUp
}

func (v *Flag) SetDown() {
	*v &^= FlagUp
}

// Another usage for iota in consts:  KiB, MiB, etc.
// Not possible for KB, MB, etc. because of the lack of an exponentiation operator :(
