package chapter03

import (
	"fmt"
	"unicode/utf8"
)

func Demostrlen() {
	strings := []string{
		"hello",
		"secrètes", // French
		"こんにちは",    // Japanese
		"హలో",      // Telugu
	}

	for _, s := range strings {
		fmt.Printf("%s: %d\n", s, len(s))
	}

	fmt.Println("Converting to a slice of runes...")

	// For the most part this works, but for Telugu it only sort of works.
	// In Telugu we have base characters and then modifiers which can change
	// how the character sounds. It's similar to how è has a base character
	// of e followed by a modifier to specify how exactly it should sound.
	// With Telugu, the modifier is a separate rune and thus counts as an
	// additional character when it really isn't.
	for _, s := range strings {
		runeRep := []rune(s)
		fmt.Printf("%s: %d\n", string(runeRep), len(runeRep))
	}

	fmt.Println("Using DecodeRuneInString...")
	// Same issue as above...
	for _, s := range strings {
		var i, c int
		runeRep := []rune(s)
		for i = 0; i < len(s); {
			_, size := utf8.DecodeRuneInString(s)
			i += size
			c++
		}
		fmt.Printf("%s: %d\n", string(runeRep), c)
	}
}
