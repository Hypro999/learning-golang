### Just a Few Quick Notes for Chapter 2.

- [Exported identifiers](https://golang.org/ref/spec#Exported_identifiers):
  An identifier may be exported to permit access to it from another package. An identifier is exported if both:
    1. the first character of the identifier's name is a Unicode upper case letter (Unicode class "Lu"); and
    2. the identifier is declared in the package block or it is a (struct) field name or method name.
  All other identifiers are not exported. 

- *Tuple Assignment*: something like `i, j = j, i` or `i, j = 0, 1`. It's not the same as multi-variable
  declaration though e.g. `i, j := 0, 1`.

- The builtin `new` function can be used to create a new unnamed variable and then return a pointer it.

- `struct{}` and `[0]int` are examples of zero-size variables (so they will be useful for when we use maps as sets).
  In gccgo if we use `new` to create zero sized variables then they will share the same addresses. In gc (the default
  go compiler) they will have different addresses.

- Variable lifetime in Go is not exactly scope based but rather reference based. To quote the book, "Because the
  lifetime of a variable is determined only by whether or not it is reachable, a local variable may out live a
  single iteration of the enclosing loop. It may continue to exist even after its enclosing function has returned."
  So this means that a function can return the address of one of it's local variables and that variable won't be
  an invalid reference (unlike in C).

- "keeping unnecessary pointers to short-lived objects within long-lived objects, especially global variables, will
  prevent the garbage collector from reclaiming the short-lived objects."

- typedef-ing with `type` can also be used to separate two distinctly different usages of the same underlying type
  so that they can't accidently be mixed. E.g. using an int to denote a timestamp vs using an int for a file descriptor.
  This works because of golang's strict rules for type safety - no implicit type conversions (except when defining consts?).
e.g.
```go
type alpha uint8
type beta uint8

func main() {
	var x alpha
	var y beta

	x = 1
	y = 2
	x, y = y, x // illegal - won't compile
	z := x + y // illegal - won't compile
}
```

- "The doc comment immediately preceding the package declaration documents the package as a whole.
  Conventionally, it should start with a summary sentence. Only one file in each package should have
  a package doc comment. Extensive doc comments are often placed in a file of their own, conventionally
  called doc.go."
