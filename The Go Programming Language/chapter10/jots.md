# Chapter 10 - Packages and the Go Tool
Just a few notes here and there. Mostly for stuff that I either haven't seen
before or want to really reiterate.

Note: This book was written before the Go Modules system was released. The Go Modules system is a
*huge* improvement over the older packages / enforced-GOPATH convention.

### 10.5 Blank Imports
- Sometimes we may need to import a package merely for the possible side-effects of doing so,
  like executing it's `init()` function. but if we don't use the imported package name, we will
  get a compile-time error (unused import). The way to get around this is to use a "blank import",
  which is quite similar to a blank assignment.

- An example of where blank imports are used is for "registration" with database drivers.
  Example:
  ```go
    import (
        "database/mysql"
        _ "github.com/lib/pq"              // enable support for Postgres
        _ "github.com/go-sql-driver/mysql" // enable support for MySQL
    )

    db, err = sql.Open("postgres", dbname) // OK
    db, err = sql.Open("mysql", dbname)    // OK
    db, err = sql.Open("sqlite3", dbname)  // returns error: unknown driver "sqlite3"
  ```

- A recent (as of writing this) example of where blank imports could prove useful is with
  the new `time/tzinfo` package (in version 1.15). It adds timezone awareness support on
  blank importing (or on using a compile-time flag).


### 10.7 The Go Tool
- To cross-compile, modify the `GOOS` and `GOARCH` variables.
  Example values:
    - GOOS: android, darwin, dragonfly, freebsd, linux, nacl, netbsd, openbsd, plan9, solaris,
   windows
    - GOARCH: 386, amd64, amd64p32, arm, arm64, ppc64, ppc64le, mips, mipsle, mips64, mips64le,
   mips64p32, mips64p32le, ppc, s390, s390x, sparc, sparc64

- "Some packages may need to compile different versions of the code for certain platforms or
  processors, to deal with low-level portability issues or to provide optimized versions of
  important routines, for instance. If a file name includes an operating system or processor
  architecture name like `net_linux.go` or `asm_amd64.s`, then the go tool will compile the
  file only when building for that target. Special comments called build tags give more
  fine-grained control."

- "the go build tool treats a package specially if its import path contains a path segment named
  internal. Such packages are called internal packages. An internal package may be imported only
  by another package that is inside the tree rooted at the parent of the internal directory."
