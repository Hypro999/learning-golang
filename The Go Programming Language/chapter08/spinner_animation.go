package chapter08

import (
	"fmt"
	"time"
)

const (
	DefaultSpinnerDelay = 100 * time.Millisecond
)

// Spinner can be run in its own goroutine while computation happens in another.
// The trick here is that the terminal will (should) interpret \r as a command
// to move the cursor back to the start of the current line so that we may start
// overwritting characters.
func Spinner(delay time.Duration) {
	for {
		for _, r := range `-\|/` {
			fmt.Printf("\r%c", r)
			time.Sleep(delay)
		}
	}
}
