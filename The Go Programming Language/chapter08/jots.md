# Chapter 8 - Goroutines and Channels
Just a few notes here and there. Mostly for stuff that I either haven't seen
before or want to really reiterate.

This chapter focuses on Go's support for the Communicating Sequential Processes
(CSP) model as enabled via. goroutines and channels. Looking at shared global
memory (the more traditional approach to concurrency) is handled in the next
chapter.


### Channels
- Channels are reference types. Just like slices and maps. It's zero value is
  nil.

- Three operations: send, receive, and close.

- To receive from a channel but not actually store the value, simply use `<-c`.

- After closing a channel, trying to send will panic and trying to receive
  will return immediately with the zero value of the channel type.
  For example:
  ```go
    func main() {
        c := make(chan bool)
        go func() {
            c <- true
            close(c)
            fmt.Println("inner goroutine done")
        }()
        for {
            x := <-c
            fmt.Println(x)
        }
    }
  ```
  will output:
  ```
    inner goroutine done
    true
    false
    false
    false
    ...
  ```

- When a channel is closed, we can detect it with a receive operation where we
  use the "ok syntax". For example: `val, ok := <-ch`.

- Alternatively, we can use a loop with the `range` keyword over a channel.
  The loop will end once the channel has been closed. This way we won't have
  to use the "ok syntax".

- "At tempting to close an already-closed channel causes a panic, as does
  closing a nil channel."

- Unbuffered channels are sometimes also called "synchronous channels".

- When sending on an unbuffered channel, then receipt of the value happens
  before the reawakening of the sending goroutine.

- When we use channels just for their blocking/synchronizing property and
  don't actually care about the data/value of the message, we call the sent
  messages "events".

- There is a concept of "pipelines" with channels (similar to bash). It's when
  the output of one goroutine is the input to another.

- "A channel that the garbage collector determines to be unreachable will have
  its resources reclaimed whether or not it is closed. (Don’t confuse this
  with the close operation for open files. It is important to call the Close
  method on every file when you’ve finished with it.)"

- Conversion (typecasting) from bidirectional channels to unidirectional
  channels is always possible. Never the other way around.

- Calling `len()` on a buffered channel will return the number of items
  buffered at that instance. Similarly, `cap()` will return the total
  buffering capacity.

- It's normal for multiple goroutines to send (or receive) values to (or from)
  the same channel concurrently.

- **goroutine leak**: When a goroutine is forever blocked/deadlocked because
  it was trying to send to a channel but nothing was ever receiving from it
  (or vice versa). Goroutines are never garbage collected so this is actually
  a pretty big type of bug.

- "channel draining".

- A really good idiom for parallel looping when WaitGroups are involved: Use
  one goroutine for performing `wg.Wait()` then `close(chn)` while the main
  goroutine will use a range-based loop on the channel to receive values.

- Whenever opening file descriptors (network connections, disk files, etc.) in
  a loop (concurrent or not) - place a restriction / control the number of
  file descriptors open at any given time. In Unix/Linux each process has a
  set maximum number of file descriptors it can keep open at any given time.
  And remember, always close your file descriptors!

- "Unbounded parallelism is rarely a good idea since there is always a
  limiting factor in the system, such as the number of CPU cores for compute-
  bound work loads, the number of spindles and heads for local disk I/O
  operations, the bandwidth of the network for streaming downloads, or the
  serving capacity of a web service"

- "We can limit parallelism using a buffered channel of capacity n to model a
  concurrency primitive called a counting semaphore." This is a common pattern
  for when we need to limit parallelism to prevent exhausting file descriptors.

- `time.Tick` will return a channel on which it will send event messages (i.e.
  sending something just for the sake of sync and not data) every unit of the
  specified time interval.

- Each "case variable" of a channel-multiplexing switch statement is called
  a "communication" (a send or a receive).

- `time.After` is similar to `time.Tick` in the sense that it returns a
  channel. The main difference is that it will return an event on the channel
  only once, right after the specified time duration.

- `time.Tick` uses a goroutine and this goroutine can't be stopped unless you
  use `time.NewTicker` instead. If you won't use a ticker for the entire
  duration of your program, and you want to avoid a goroutine leak, then call
  `ticker.Stop()`.

- Polling channels in Go:
    ```go
        switch {
        case <-mychan:
            // Do something
        default:
            // Do nothing
        }
    ```


### 8.9 Cancellation
- "There is no way for one goroutine to terminate another directly, since that
  would leave all its shared variables in undefined states."

- There's an idiomatic approach to "cancelling" goroutines in a "broadcast"
  kind of manner:
    ```go
        var done = make(chan struct{})

        func cancelled() bool {
            select {
            case <-done:
                return true
            default:
                return false
            }
        }
    ```
  Here, we exploit the fact that once a channel has been closed, subsequent
  read operations will return immediately. So unlike with the "abort" example
  they gave earlier in the textbook where we sent a value on the channel to
  indicate termination, here we close the channel itself.

- To drain goroutines using a given channel, use the `for range myChan`
  loop/syntax with an empty body.

- "Cancellation involves a trade-off; a quicker response of ten requires more
  intrusive changes to program logic. Ensuring that no expensive operations
  ever occur after the cancellation event may require updating many places in
  your code, but often most of the benefit can be obtained by checking for
  cancellation in a few important places."

- Using select to perform polling is a pretty good way to do a "non-blocking
  read/write" so if we need to enforce priority then we can try too perform
  the reads/writes as a series of non-blocking operations instead of using a
  single select statement.
