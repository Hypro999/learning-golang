package chapter04

import (
	"fmt"
	"os"
	"text/template"
)

// Example inspired by https://blog.gopheracademy.com/advent-2017/using-go-templates/
// which does a better job of explaining the basics of templates than the book does.

const mytemplate = `My ToDo List:{{ range . }}
  {{ .Name }}
    {{ .Description }}
{{ end }}`

type ToDoItem struct {
	Name        string
	Description string
}

func DemoTextTemplates() {
	todolist := []ToDoItem{
		{
			Name:        "Build stuff",
			Description: "Build a cool headquarters in the nether.",
		},
		{
			Name:        "Create a strider stable",
			Description: "Make small lava pools with slabs for steps.",
		},
	}

	newTemplate := template.New("ToDo List")
	parsedTemplate, err := newTemplate.Parse(mytemplate)
	// template.New("").Parse() is more idiomatic.
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	err = parsedTemplate.Execute(os.Stdout, todolist)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
