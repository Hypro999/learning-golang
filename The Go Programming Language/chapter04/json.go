package chapter04

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type Person struct {
	Name     string `json:"full_name"`
	DOB      string `json:"date_of_birth,omitempty"`
	PassHash string `json:"-"` // This should never be JSONified despite being a exported field.
}

func DemoJsonMarshaling() {
	// A basic slice of ints.
	data0 := []int{1, 2, 3, 4, 5}
	payload, _ := json.MarshalIndent(data0, "", "    ")
	fmt.Println(string(payload))

	data1 := map[string]int{
		"one":   1,
		"two":   2,
		"three": 3,
	}
	payload, _ = json.MarshalIndent(data1, "", "    ")
	fmt.Println(string(payload))

	data2 := Person{
		Name:     "Hypro999",
		DOB:      "",
		PassHash: "6b3a55e0261b0304143f805a24924d0c1c44524821305f31d9277843b8a10f4e", // Easy...
	}
	payload, _ = json.MarshalIndent(data2, "", "    ")
	fmt.Println(string(payload))

	data3 := Person{
		Name:     "Shinya Kogami",
		DOB:      "2084/16/08",
		PassHash: "831d04d8d242df1de64fa15e43a52508167057f646ad681541c58be4b19828e1", // It's a UUID, don't bother.
	}
	payload, _ = json.MarshalIndent(data3, "", "    ")
	fmt.Println(string(payload))
}

func DemoJsonUnmarshaling() {
}

func DemoJsonUnmarshalingXKCD() {
	url := "https://xkcd.com/571/info.0.json"
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		fmt.Printf("Unsuccessful response code: %d\n", resp.StatusCode)
		os.Exit(1)
	}

	var comicData struct {
		Month     string
		Year      string
		SafeTitle string `json:"safe_title"`
		Day       string
	} // anonymous/unnamed struct.
	// Why aren't we using ints here?
	// It's cause XKCD isn't returning an int. It's returning a string... *facepalm*

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	err = json.Unmarshal(body, &comicData)
	if err != nil {
		fmt.Println(err)
		fmt.Println(string(body))
		os.Exit(1)
	}

	fmt.Printf("%s/%s/%s %q\n", comicData.Year, comicData.Month, comicData.Day, comicData.SafeTitle)
}

// TODO: Demo json.Encoder and json.Decoder.
