# Chapter 4 - Composite Types
Just a few notes here and there. Mostly for stuff that I either haven't seen
before or want to really reiterate.


### 4.1 Arrays
- How to create an array literal without explicitly specifying the length:
  ```go
  q := [...]int{1, 2, 3}
  ```

- Combining `consts` (optionally using `iota`) and an array can make for
  a good fixed dictionary. For example:
  ```go
  type Token int 

  const (
      GT  Token = iota
      LT
      GTE
      LTE
  )

  symbol := [...]string{GT: ">", LT: "<", GTE: ">=", LTE: "<="}
  ```
  Notice that we can use labels to specify exactly which index gets what value.

- One of the only times arrays are actually used is for SHA256 digests which
  are 256 bits long (32 bytes). Thus we'd use `[32]byte`.

- Go, like C, does not pass arrays by reference. We can always pass an array
  pointer if we want to pass by reference.

- Arrays can be comparable. If an array consists of comparable elements then
  the array itself can be compared to other arrays of the same type and size.


### 4.2 Slices
- Nothing new, but just remember that slices are always connected to an
  underlying array and that multiple slices can share the same underlying
  array. For example,
  ```go
  arr := [...]int{1, 2, 3, 4, 5}
  x1 := arr[0:2]  // [1, 2]
  x2 := arr[2:4]  // [3, 4]
  x3 := arr[4:]   // [5]
  ```
  here, all of the slices share the same underlying array. Thus `x1` has a
  capacity of 5 since it has the rest of the array to expand into looking
  forward. Similarly `x2` has a capacity of 3, then `x1` has a capacity of 1.

- Slicing beyond `cap()` causes a runtime panic. Slicing beyond `len()` just
  expands the slice as long as it's less than `cap()`.

- Slices in go are reference types so that means that something like:
  ```go
  arr := [...]int{1, 2, 3, 4, 5}
  x1 := arr[0:2] // [1, 2]
  x2 := x1
  x2[0] = 100
  ```
  Would result in `x2` simply being an *alias* for `x1`. When we change
  something in `x2`, `x1` and `arr` are also changed because `x1` and `x2`
  are simply references to a certain part of `arr`.

- But since arrays are not reference types, something like:
  ```go
  arr := [...]int{1, 2, 3, 4, 5}
  shadow := arr
  shadow[0] = 100
  fmt.Printf("arr: %v\nshadow: %v\n", arr, shadow)
  ```
  would mean that `shadow` would be an independent copy of `arr`. So if we
  changed `shadow`, `arr` would remain unchanged.

- Slices are incomparable no matter what. Though `[]byte`s can be compared
  with the `bytes.Equal` function, which is specially optimized. The only
  legal slice comparison is against `nil` which is the zero value of a slice.

- `copy(dst, src)` is sort of like "dst = src" for slices - as a replacement
  to an explicit loop copying over elements (covered before, but this explains
  why the arguments might feel like they're in reverse order.

- Based on what we learn about slices just be references/pointers, the
  following code won't change the contents of the slice `li`:
  ```go
  func foo(list []int) {
  	list = append(list, 3)  // The reference is just getting re-assigned.
  }
  
  func main() {
  	li := []int{1, 2}  // 2 is a power of 2 so this slice is at max capacity.
  	foo(li)
  	fmt.Printf("%d %v\n", cap(li), li) // Has not changed.
  }
  ```

- A nice idiomatic way to remove an element from the middle of the slice while
  preserving the order of the elements would be to use `copy` on the later
  half.
  ```go
  func remove(slice []int, i int) []int {
      copy(slice[i:], slice[i+1:])
      return slice[:len(slice)-1]
  }
  ```


### 4.3 Maps
- Using floats as keys for maps is generally a bad idea since even if they are
  comparable, precision issue might lead to unintended key collisions.

- The zero value for maps is nil since maps, like slices, are a reference
  type. This is why if you declare a null map with something like
  `var mymap map[string][int]`, you won't be able to add any new key/value
  pairs - because this is a map reference pointing to `nil`.
  But if you used `mymap := map[string]int{}` you could add keys since here
  the map reference is pointing to an empty map, not `nil`.

- "Most operations on maps, including lookup, delete, len, and range loops,
  are safe to perform on a nil map reference, since it behaves like an empty
  map. But storing to a nil map causes a panic."

- Maps in Go are similar to `defaultdicts` in Python because if a key does
  not exist, then the zero value of the map's values' type will be returned.
  So in the following example even if the key `xkcd` didn't exist, this
  would not be erroneous.
  ```go
  comics_count := map[string]int{}
  comics_count["xfce"] = comics_count["xfce"] + 1
  ```
  `comics_count["xfce"]` would be 1.

- The following are also syntactically valid ways to operate on map elements.
  ```go
  comics_count["xfce"] += 1
  comics_count["xfce"]++
  ```

- Map elements are not variables so we can't access their addresses.
```go
_ = &comics_count["xfce"]
```
is invalid and would raise a compile time error.

- When iterating over a map, we don't have to iterate over both keys and
  values. We can also iterate over just the keys (useful for maps as sets),
  i.e. we don't need to use the throwaway variable (`_`).

- Turns out that something like `make([]string, 0, len(x))` is actually a
  common practice.

- When using Tuple assignment to see if a key exists in a map, the second
  variable is apparently usually called "ok" and not "exists". A quick look
  at the Go source code (`go/ast/imports.go`) (written in Go itself) confirms
  this statement.

- When dealing with keys that are non-comparable, like slices, we'd need to
  define our own additional hash function to hash the non-comparable type to
  something that actually is comparable. Then we use it as an intermediate
  step before accessing the map.


### 4.4 Structs
- Each value in a struct is called a `field`, not an `attribute` or something
  else.

- "Field order is significant to type identity".

- Since the struct type definition is very long, specifying it like a literal
  again and again is just not practical, that's why we always typedef them.
  This is not needed in C since C has named structs by default.
  One advantage to structs in Go not always needing a name is being able to
  define temporary structs on-the-fly which can be useful for certain temp-use
  cases like unmarshaling small JSON payloads.

- Struct fields that begin with a capital letter are exported (otherwise they
  are not exported). Unexported fields cannot be directly accessed outside of
  the package the struct belongs to. If each struct is given it's own package
  then unexported fields are basically "private" members while exported
  fields are like "public" members. So then you could enforce getters/setters.

- Structs are aggregate types like arrays, not reference types like slices.
  So the zero value for a struct is composed of the zero value for all of
  it's fields.

- `struct{}` is the empty struct and it has a size of zero (good for being the
  value in maps as sets). The empty interface `interface{}` seems to be used
  more often though as it's synonymous with `void *` from C. More information
  will be covered in chapter07 which is specifically dedicated to interfaces.

- Structs literals have two initialization constructs one is when explicit
  labels are used to specify which field gets what label and the other is
  where all of the field values are assigned in order. The latter is not
  possible outside of the package the struct came from if the struct has
  unexported fields.

- Combining struct literal initialization with the "address of" operator (&)
  is not good idea for large structs that will often be passed around various
  functions.

- If all of the fields in a given struct are comparable then the instances of
  the struct themselves are also comparable. Comparable struct types can be
  used as keys for a map because of their comparable nature.


##### 4.4.3 Struct Embedding and Anonymous Fields
Covered quite briefly in "Introducing Go", yet this is pretty important. So it
deserves it's own independent sub-section.

- With "struct embedding" we can embed one struct's data members into another
  struct as an "anonymous field". This gives us a bit of "syntactic sugar" in
  using dot notation by reducing the depth of the dots during composition of
  structs.

- "Because 'anonymous' fields do have implicit names, you can’t have two
  anonymous fields of the same type since their names would conflict."

- Example in `./embedded.go`.

- Same rules for unexported fields still remains even with embedding.

- One more advantage to using embedding: "the outer struct type gains not just
  the fields of the embedded type but its methods too." and to continue this
  nice elucidation from the textbook, "This mechanism is the main way that
  complex object behaviors are composed from simpler ones. Composition is
  central to object-oriented programming in Go."

### 4.5 JSON
- In JSON, a unicode code point translates to UTF-16 not 32-bit UTF-8 encoded
  runes. JSON supports the same unicode code point syntax of `\Uhhhhh` though.

- Go data structures can be marshaled into JSON and unmarshaled vice versa.

- `json.Marshal` from the `encoding/json` package will take a Go data
  structure and covert it into a byte slice containing the JSON data in a
  compact form. To get it in an indented form instead use `json.MarshalIndent`.

- Structs can be given special "field tag"s of the format `json:"name"`
  that will be used by the JSON marshaling code to determine the name the key
  should assume in JSON representation.
  Field tags are a way of associating metadata with fields of structs at
  compile time.
  Example:
  ```go
  type Person struct {
      Name string `json:"full_name"`
      DOB string `json:"date_of_birth"`
  }
  ```

- Only exported fields of structs will be marshaled.

- "A field tag may be any literal string, but it is conventionally
  interpreted as a space-separated list of key:"value" pairs; since they
  contain double quotation marks, field tags are usually written with raw
  string literals" (i.e. using backticks instead of regular quotation marks).

- The Go library for JSON uses "reflection" (a special Go technique for
  allowing Go code to analyze Go code itself; a sort of meta-analysis) to read
  field tags. Go also makes use of `interface{}` to be able to take a wide
  variety of data structures including slices, maps, and structs.

- There are a few more special field tag related features that the Go JSON
  encoding package offers (like "-" and "name,omitempty"). Refer to
  `./json.go` for examples. Different packages use the contents of struct
  field tags in different ways.

- When unmarshaling into a struct only the struct fields will be filled while
  everything else will be ignored. This can be pretty convenient for keeping
  only the data that we're interested in.

- "the matching process that associates JSON names with Go struct names during
  unmarshaling is case-insensitive, so it’s only necessary to use a field
  tag when there’s an under-score in the JSON name but not in the Go name."

- `json.NewEncoder` and `json.NewDecoder` will create new encoder and decoder
  instances (structs) which will implement the Writer and Reader interfaces
  respectively.


### 4.6 Text and HTML Templates
- Go has some standard template libraries (No, I'm not talking about C++ STL,
  rather more like Python Jinja). `text/template` and `html/template` are two
  good examples. 

- Template language expressions in templates are called "actions". Actions are
  parts of template strings denoted by content enclosed in double brackets,
  e.g.  `{{...}}`.

- "Actions" may be variables, struct members, function/method calls, control
  flow statements, loop statements, etc.

- "Within an action, the | notation makes the result of one operation the
  argument of another, analogous to a Unix shell pipeline."

- `Funcs` can be used to provide what functions can be accessed by the
  template. For example:
  ```go
  template.New("").Funcs(template.FuncMap{"daysAgo": daysAgo}).Parse(templ)
  ```

- The `html/template` package protects against injection attacks like XSS.

From https://blog.gopheracademy.com/advent-2017/using-go-templates/:

- The three most important and most frequently used functions are:
  - New: allocates new, undefined template.
  - Parse: parses given template string and return parsed template.
    - alternative: ParseFiles
  - Execute: applies parsed template to the data structure and writes result
   to the given writer.
    - alternative: ExecuteTemplate
  - Must: Auto-error handling around New+Parse/ParseFiles

- "template packages provide the Must functions, used to verify that a
  template is valid during parsing." Similar to MustCompile from `regexp`.

