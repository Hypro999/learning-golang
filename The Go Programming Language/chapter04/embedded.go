package chapter04

import "fmt"

type Point struct {
	X, Y int
}

type Circle struct {
	Point  // anonymous and embedded
	Radius int
}

type Wheel struct {
	Circle // anonymous and embedded
	Spokes int
}

func DemoEmbeddedStructs() {
	// The literal intializer syntax sadly won't be simplified though.
	wheel := Wheel{
		Circle{
			Point{4, 4},
			4,
		},
		6,
	} // literal intialization syntax not using explicit labels.
	fmt.Printf("%d\n", wheel.X)              // The syntatic sugar way.
	fmt.Printf("%d\n", wheel.Circle.Point.X) // The longer way which is still valid.
	fmt.Printf("%#v\n", wheel)
}
