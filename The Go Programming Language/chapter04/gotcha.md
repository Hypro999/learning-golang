Excerpts from: https://blog.golang.org/slices-intro

Re-slicing a slice doesn't make a copy of the underlying array. The full array will be kept in
memory until it is no longer referenced. Occasionally this can cause the program to hold all the
data in memory when only a small piece of it is needed. To fix this problem one can copy the
interesting data to a new slice before returning it.
