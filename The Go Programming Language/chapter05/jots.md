# Chapter 5 - Functions
Just a few notes here and there. Mostly for stuff that I either haven't seen
before or want to really reiterate.


### 5.1 Function Declarations
- No concept of default parameter values. Thus since there was no need, it
  also doesn't allow specifying the parameter names during a function call
  like python does (kwargs).

- A function declaration without a body indicates that the function is
  implemented in a different language (i.e. assembly routines).


### 5.2 Recursion
- "The golang.org/x/... repositories hold packages designed and maintained by
  the Go team for applications such as networking, internationalized text
  processing, mobile platforms, image manipulation, cryptography, and
  developer tools. These packages are not in the standard library because
  they’re still under development or because they’re rarely needed by the
  majority of Go programmers."

- "Go implementations use variable-size stacks that start small and grow as
  needed up to a limit on the order of a gigabyte. This lets us use recursion
  safely and without worrying about overflow.


### 5.3 Multiple Return Values
- Wrapping a function with `log.Println()` can be useful for debugging.

- Returned values can be named in the function declaration. This is useful for
  self-documenting code.

- When naming the returned values, you can perform a `bare return` which means
  that you can just use `return` without having to explicitly specify what
  you're returning in the return statement (since it can now be inferred from
  the function declaration). Bare returns don't have to be used though when
  using named return variables. The book discourages it but TBH, I think that
  they're pretty cool.


### 5.4 Errors
- "Go programs use ordinary control-flow mechanisms like if and return to
  respond to errors. This style undeniably demands that more attention be
  paid to error-handling logic, but that is precisely the point."

- Common error handling strategies:
    1. Propagate the error (only possible with functions).
    2. Propagating with chaining.
   When propagating across multiple functions, have each one add a bit more
   information using `fmt.Errorf`. "provide a clear causal chain from the root
   problem to the overall failure". Example of propagating with chaining:
    "genesis: crashed: no parachute: G-switch failed: bad relay orientation"
    3. Retrying the failed operation, potentially with a delay in between.
   Try a certain number of times before giving up (an infinite number of
   reties is usually a bad idea). Try stuff like exponential growth in wait
   time.
    4. Print an error and exit altogether. Maybe try using `log.Fatalf`. In
   general, the log package has a bunch of useful stuff.
    5. For errors can sorta be ignored, just log and continue.
    6. Do absolutely nothing and just continue.

- The EOF error can be found in the IO package as `io.EOF`. So we can just
  compare with this instead of making `errors.New("EOF")` again (which is
  exactly what `io.EOF` is anyways).


### 5.5 Function Values
- The zero type value of a function is `nil`.

- Functions are not a comparable type.

- `strings.Map` just like Python's map function.

- Using function pointers and passing them to function calls, we can achieve
  general-ish functions. For example, we can make a binary tree traversal
  function that applies an operation to each node that it traverses. The user
  can pass this operation as a pointer to a function when calling this
  traversal function.


### 5.6 Anonymous Functions
- "Function values are not just code but can have state". See `generatorDemo`
  From `../../Introducing Go/chapter06/functions.go` to see what is means.
  It's a pretty cool concept.

- Functions are reference types.

- "A `closure` is a function value that references variables from outside its
  body." (https://tour.golang.org/moretypes/25)

- Using closures we can avoid having to make a separate "internal use"
  function if we won't be reusing it elsewhere. This is generally useful for
  functions employing recursion where the first step is different from the
  remaining steps.

- Closures are also useful in recursion when we want to modify some kind of
  "global" variable without having this variable actually being global or
  being constantly passed around. For a rough example:
  ```go
  func processInput() {
    state := 0
    stateMachine := func () {
        // Function body relying on state. Might be recursive in which case
        // this approach is more useful.
    }
    return
  }
  ```

- On an unrelated note: `Topological Sorting` is pretty cool. Look at the DFS
  approach taken in the textbook.

- `resp.Request.URL.Parse(link)` a way to convert relative links to absolute
  links w.r.t the response `resp`. If the link is already absolute, then it
  won't be affected. Instead of creating a string, it will return a pointer
  to a URL struct instance.

- IMPORTANT: Remember that when defining (anonymous) functions inside a loop
  we must always pass the loop variable(s) as an argument to the function or
  make a copy of the variable and use this copy inside the function to avoid
  letting the loop iteration change the value of the variable.
  If we don't do this, all of the functions will end up sharing the same
  variable (the loop variable) - and in the end a variable is an addressable
  storage location, not a value. But by defining a new variable in each
  iteration, each function uses its own unique variable. The loop variable's
  storage location is fixed across iterations despite the misleading `:=`
  symbol.
  We call this phenomenon "loop variable capture".


### 5.7 Variadic Functions
- Variadic parameters to functions seem like slices but they are different.
  They are their own type.
(nothing else new)


### 5.8 Deferred Function Calls
- Deferred function calls are executed in the reverse order they were deferred.

- Apart from using defer to close resources, it can also be used to create a
  profiler, or on-entry/on-exit type function. Example:
  ```go
    func Profile(name string) func() {
        start := time.Now()
        return func() {
            log.Printf("Time elapsed for %s: %s", name, time.Since(start))
        }
    }
    ...
    defer Profile("myFoo")()
  ```
  Another way to profile is to log function entry and exit.

- "Deferred functions run after return statements have updated the function’s
  result variables. Because an anonymous function can access its enclosing
  function’s variables, including named results, a deferred anonymous function
  can observe the function’s results." => This is an extension of the above
  point.


### 5.9 Panic
(nothing new worth noting here...)


### 5.10 Recover
- "If the built-in recover function is called within a deferred function and
  the function containing the defer statement is panicking , recover ends the
  current state of panic and returns the panic value." Example of the deferred
  function:
```go
    defer func() {
        if p := recover(); p != nil {
            err = fmt.Errorf("internal error: %v", p)
            // This err variable is assumed to be returned by the
            // function using this anonymous inner function.
        }
    }()
```

- One should rarely recover from a panic. The returned partial results are often
  not well-defined or documented and sometimes critical tasks might be repeatedly
  skipped (e.g. not closing FDs). Also important bugs may go unnoticed.

- Important best practices:
    - Don't recover from another package's panics (esp. Third party ones).
    - "Similarly, you should not recover from a panic that may pass through a
   function you do not maintain, such as a caller-provided callback, since you
   cannot reason about its safety."
    - (more info in the textbook).

