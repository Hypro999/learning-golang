package chapter05

import "fmt"

/* Demonstrate how the scope of loop variables is special and thus sort of
 * like an edge case. When using loop variables in anonymous functions, we
 * should define a new variable inside the loop and have the function use
 * that. For anonymous goroutines, we can pass the loop variable as an
 * argument to the goroutine function and that would work too. */
func DemoScopeEdgeCase() {

	// Demo 1. Doing it the wrong way. Here i is outside the scope of the loop.
	var i int
	methods := []func(){}
	for i = 0; i < 2; i++ {
		methods = append(methods, func() {
			fmt.Println(i)
		})
	}
	for _, method := range methods {
		method()
	}

	fmt.Println()

	// Demo 2. Also doing it the wrong way. Here j belongs to the scope of the
	// loop, but despite what the `j :=` part suggests, we're not creating
	// j again and again. It's actually more like we create it for the first
	// iteration and then in later iterations just update the value at the
	// location j refers to. So in the end all of the functions created are
	// using the same j.
	methods = []func(){} // Point methods to a new, empty slice.
	for j := 0; j < 2; j++ {
		methods = append(methods, func() {
			fmt.Println(j)
		})
	}
	for _, method := range methods {
		method()
	}

	fmt.Println()

	// Demo 3: Doing it the right way. In each loop iteration we are actually
	// re-creating m again and again - i.e. we are alloting a new address each
	// time and storing the current value of k there.
	methods = []func(){} // Point methods to a new, empty slice.
	for k := 0; k < 2; k++ {
		m := k
		methods = append(methods, func() {
			fmt.Println(m)
		})
	}
	for _, method := range methods {
		method()
	}

}
