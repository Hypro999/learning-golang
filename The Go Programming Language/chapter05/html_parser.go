package chapter05

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"golang.org/x/net/html"
)

type Anchor struct {
	Href string
}

func walkLookingForAnchors(cursor *html.Node, anchors []Anchor) []Anchor {
	// Note: This won't extract all links possible. Just the links present
	// in anchors.
	if cursor.Type == html.ElementNode && cursor.Data == "a" {
		for _, a := range cursor.Attr {
			if a.Key == "href" {
				value := strings.TrimSpace(a.Val)
				if value != "" {
					anchors = append(anchors, Anchor{value})
				}
			}
		}
	}
	// Depth First Search.
	for cursor = cursor.FirstChild; cursor != nil; cursor = cursor.NextSibling {
		anchors = walkLookingForAnchors(cursor, anchors)
	}
	return anchors
}

func findAnchors(src io.Reader) (anchors []Anchor) {
	root, err := html.Parse(src)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// anchors is a slice and since, unlike arrays, slices are reference type
	// objects, this is technically a pass by reference (pass by object
	// reference I guess).
	anchors = walkLookingForAnchors(root, make([]Anchor, 0))
	return // Example of a bare return.
}

func findAnchorsFromPage(url string) []Anchor {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		// If this function were part of a library we should have just
		// returned this error instead of ending the program.
		fmt.Printf("Unsuccessful request: %d\n", resp.StatusCode)
		os.Exit(1)
	}
	return findAnchors(resp.Body)
}

func PrintAnchorsFromPage(url string) {
	anchors := findAnchorsFromPage(url)
	for i, anchor := range anchors {
		fmt.Printf("%d: %s\n", i, anchor.Href)
	}
}
