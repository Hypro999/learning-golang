package chapter05

import (
	"fmt"
	"log"
	"time"
)

func DeferredAnonymousDemo() (x int) {
	defer func() {
		fmt.Println(x)
	}()
	x = 1
	return
}

func Profile(name string) func() {
	start := time.Now()
	return func() {
		log.Printf("Time elapsed for %s: %s", name, time.Since(start))
	}
}
