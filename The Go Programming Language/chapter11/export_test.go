package chapter11

// Note: We really did not need to call this export_test.go, we really could
// have called it just about anything we wanted (as long as it ended in _test).
// Heck, we could have just reused internal_source_test. But let's try and follow conventions.

// This will only be exported when tests are run because this file ends in _test.
// So during tests (and only during tests), this variable (and hence the internal
// boo function) will be visible to EVERY package out there importing chapter11.
var BackdoorToBoo = boo
