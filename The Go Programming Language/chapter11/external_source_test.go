package chapter11_test

import (
	"testing"
	"thegopl/chapter11"
)

// Note: I could have called this package anything I wanted.I went with chapter11_test just cause
// it's convention and I couldn't think of a better name :P.

// This function's name DOES need to start with Test[A-Z]* though. It's still a test and still
// needs to be recognized by Go's test runner (also needs to accept t *testing.T).
func TestBooFromTheOutside(t *testing.T) {
	// The same as TestBoo, because this example is really simple and is more about
	// hacks around the visibility.
	b := chapter11.BackdoorToBoo()
	// This is made available because export_test.go (and any other _test.go file) will be included by the test_runner when running tests.
	if !(b == "boo!") {
		t.Errorf("boo() = %s; want \"boo!\"", b)
	}
}
