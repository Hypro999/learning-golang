package chapter11

import (
	"fmt"
	"testing"
)

func TestBoo(t *testing.T) {
	b := boo()
	if !(b == "boo!") {
		t.Errorf("boo() = %s; want \"boo!\"", b)
	}
}

// Run with go test -v -run=Example
// or just go test -v
// Generally examples would be written for exported functions
// that would be used in other packages...
func Example() {
	fmt.Println(boo())
	// Output:
	// boo!
}
