package main

import (
	"fmt"
	"thegopl/chapter11"
)

// This will fail because the _test.go files like export_test.go will only be included
// when running the test suite. So BackdooToBoo actually won't be defined till we run
// something like go test.

func main() {
	boo := chapter11.BackdoorToBoo()
	fmt.Println(boo)
}
