#!/bin/bash

go test -v -run=NONE -bench=. -cpuprofile=cpu.out
go tool pprof -text -nodecount=10 ./chapter11.test cpu.out
rm cpu.out chapter11.test