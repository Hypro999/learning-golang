package chapter01

import (
	"fmt"
	"os"
)

func ArgsDemo() {
	for i, arg := range os.Args {
		fmt.Printf("%d: %s\n", i, arg)
	}
}
