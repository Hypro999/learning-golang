package chapter01

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

type Load struct {
	url       string
	body      string
	timeTaken float64
}

func fetch(url string) string {
	response, err := http.Get(url)
	if err != nil {
		fmt.Printf("Failed to fetch %s: %s\n", url, err.Error())
		os.Exit(1)
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Printf("Failed to read the body for %s: %s\n", url, err.Error())
		os.Exit(1)
	}

	return string(body)
}

func Fetch(urls []string) {
	c := make(chan Load)
	for _, url := range urls {
		go func(url string, c chan<- Load) {
			start := time.Now()
			body := fetch(url)
			elapsedTime := time.Since(start).Seconds()
			c <- Load{url, body, elapsedTime}
		}(url, c)
	}
	for i := 0; i < len(urls); i++ {
		load := <-c
		fmt.Printf("Loaded %s in %f seconds.\n", load.url, load.timeTaken)
	}
}
