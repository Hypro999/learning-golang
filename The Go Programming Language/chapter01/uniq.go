// Based on the original UNIX tool uniq.
// Filter adjacent matching lines from INPUT (or standard input), writing to OUTPUT (or standard output).

package chapter01

import (
	"bufio"
	"fmt"
	"os"
)

func _unique(fp *os.File) {
	uniqueLinesSet := make(map[string]bool) // Using maps to achieve sets - an idiomatic hack.
	// Scanner provides a convenient interface for reading data such as a file of newline-delimited lines of text.
	// Similar to `java.util.Scanner`.
	stdinScanner := bufio.NewScanner(fp)
	for stdinScanner.Scan() {
		line := stdinScanner.Text()
		if _, exists := uniqueLinesSet[line]; !exists {
			uniqueLinesSet[line] = true
			fmt.Println(line)
		}
	}
}

func _uniq(fp *os.File) {
	var lastLine string
	stdinScanner := bufio.NewScanner(fp)
	for stdinScanner.Scan() {
		line := stdinScanner.Text()
		if line != lastLine {
			// We will skip the first instance of a new/blank line.
			fmt.Println(line)
		}
		lastLine = line
	}
}

func Uniq() {
	argc := len(os.Args)
	if argc > 1 {
		// Then we were given some files and need to open them one by one,
		// and perform _uniq on each one of them.
		for _, filename := range os.Args[1:] {
			file, err := os.Open(filename)
			if err != nil {
				panic(err)
			}
			defer file.Close()
			_uniq(file)
		}
	} else {
		_uniq(os.Stdin)
	}
}
