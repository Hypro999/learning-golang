package chapter01

// A simple websever that just logs requests.

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

// ServerAddr is the address the server will be listening at.
const ServerAddr = "0.0.0.0:8080"

func logRequest(r *http.Request) {
	fmt.Printf("%s %s\n", r.Method, r.URL)
	bodyReader := r.Body
	body, err := ioutil.ReadAll(bodyReader)
	if err != nil {
		fmt.Println("Could not read request body.")
		return
	}
	fmt.Println(string(body))
}

func handler(w http.ResponseWriter, r *http.Request) {
	logRequest(r)

	w.Header().Add("Content-Type", "application/json")

	type Payload struct {
		Content string `json:"content"`
	}

	content := "Received"

	json.NewEncoder(w).Encode(Payload{
		Content: content,
	})
}

func runServer() {
	fmt.Printf("Starting webserver at %s\n", ServerAddr)
	http.HandleFunc("/", handler)
	err := http.ListenAndServe(ServerAddr, nil)
	fmt.Println(err)
	os.Exit(1)
}
