package chapter07

import (
	"fmt"
	"sort"
)

type namelist []string

func (li *namelist) Len() int {
	return len(*li)
}

func (li *namelist) Less(i, j int) bool {
	return (*li)[i] < (*li)[j]
}

func (li *namelist) Swap(i, j int) {
	(*li)[i], (*li)[j] = (*li)[j], (*li)[i]
}

func DemoSortInterface() {
	names := namelist{"bob", "alice", "eve", "carol"}
	sort.Sort(&names)
	fmt.Println(names)
}
