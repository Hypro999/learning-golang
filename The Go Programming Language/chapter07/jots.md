# Chapter 7 - Interfaces
Just a few notes here and there. Mostly for stuff that I either haven't seen
before or want to really reiterate.

### 7.1 Interfaces as Contracts
- Unlike all of the other types discussed so far which were "concrete",
  interfaces are an "abstract" type.

- `fmt.Printf` and `fmt.Sprintf` are both wrappers around `fmt.Fprintf` which
  takes an object implementing `io.Writer` and simply writes to it.

- *substitutability*: A feature of OOP where objects of any type satisfying a
  particular interface can be passed as an argument to methods accepting that
  interface, regardless of the exact type of the object itself.


### 7.2 Interface Types
- We can define interface types as combinations of other interface types using
  syntax similar to struct embedding. For example, `io.ReadWriter` is defined
  as:
    ```go
    type ReadWriter interface {
        Reader
        Writer
    }
    ```
  Aptly, this is called "embedding an interface".


### 7.3 Interface Satisfaction
- Take a look at `../chapter06/simple_demos.go` namely, the `SimpleMethodDemo`
  function. Notice how when we print a variable (named `a`) of type `Point`
  (not `*Point`) it's not recognized as satisfying the `fmt.Stringer`
  interface?
  That's because `Point` and `*Point` are not really the same type. The reason
  why `Point` variables can implicitly access `*Point` receiver methods is
  just because of a bit of sweet syntactic sugar enabled via. language
  specification and thus the compiler. The fact that these types are
  semantically different still remains. This variable of type Point don't
  actually satisfy the `fmt.Stringer` interface.

- The `godoc -analysis=type` tool will print the method of each type and the
  relationships between interfaces and concrete types.

- The *empty interface*: `interface{}`. Everything satisfies this so in a way
  the empty interface plays a role similar to `void *` from C (that is, the
  role of being a super generic type that can point to or represent anything).

- To get the value back out of an empty interface, we'd need to use *type
  assertions*.

- Syntax for explicit type conversions in Golang is the same as in C.


### 7.4 Parsing Flags with flag.Value
 (Just look at the example code, nothing to really note down here). 


### 7.5 Interface Values
- There are two parts to any instance of an interface (i.e. any variable
  where the type is an interface instead of a concrete type like
  `var w io.Writer`). These instances are called "interface values.

- Interface values can't completely be handled at compile time. Calling
  methods through an interface needs to be done through *dynamic dispatch*.

- To enable dynamic dispatch, interface values are comprised of two parts.
  The first part is called the *dynamic type* and it holds information about
  what concrete type the interface is currently holding. The second is the
  *dynamic value* which holds the value for that type.

- "Instead of a direct call, the compiler must generate code to obtain the
  address of the method from the type descriptor, then make an indirect call
  to that address. The receiver argument for the call is a copy of the
  interface’s dynamic value".

- Interface values can actually be compared with `==` and `!=`. Both the
  dynamic type and the dynamic value have to be equal (and the dynamic type
  has to be a comparable type in the first place, else a panic will be raised).

- Interfaces are the only type that are "risky" to compare. All other values
  are either safely comparable or not comparable at all - this is handled at
  compile time. Comparing interface values on the other hand may raise a
  runtime panic.

- To print the dynamic type of an interface via. `fmt` methods, use the "%T"
  verb. This is done internally using "reflection".

- IMPORTANT: A nil interface is not the same as an interface containing a `nil`
  pointer. `var b io.Writer = new(bytes.Buffer)` is a case of the latter. If
  we did `b == nil` we would get false! Another similar example is:
    ```go
        var p fmt.Stringer = new(Person)
        fmt.Println(p == nil)  // False
    ```
  Here, while the dynamic value is a pointer to `nil`, the dynamic type is
  still `Person`. Here, `p` is a non-nil interface value containing a nil
  pointer value.
  The example in the book is essentially:
    ```go
        // When debug == true
        var p *bytes.Buffer
        p = new(bytes.Buffer)
        m := io.Writer(p)
        fmt.Println(m == nil) // False

        // When debug == false
        var q *bytes.Buffer
        m = q
        fmt.Println(m == nil) // False

        // What's going on in the correct way
        var r io.Writer
        m = r
        fmt.Println(m == nil) // True
    ```


### 7.6 Sorting with the sort.Interface
- The sort.Interface interface:
    ```go
        package sort

        type Interface interface {
            Len() int
            Less(i, j int) bool // i, j are indices of sequence elements
            Swap(i, j int)
        }
    ```

- Follow up with a call to `sort.Sort(data sort.Interface)`. The interface
  methods handle the "custom comparator function" and other necessary stuff
  for the quicksort algorithm to be used generically.

- To sort a slice of strings, we don't need to comply with this interface.
  Instead the `sort` package offers us a helper function: `sort.Strings()`.

- Use `sort.Reverse` for generic reverse sorting.

- Use `sort.Stable` to sort while preserving the original order or equal
  elements.


### 7.7 The http.Handler Interface
- In general, the `net/http` package has pretty good documentation so I'm not
  going to write too much about it. Just refer to the docs.

- The http.Handler interface:
    ```go
        package http

        type Handler interface {
            ServeHTTP(w ResponseWriter, r *Request)
        }
    ```
  Seems pretty simple; only 1 methods is needed. This one method will take a
  `http.ResponseWriter` and a `http.Request` and will act accordingly. It
  doesn't have to return anything since it will (optionally) read from the
  request and then write to the response-writer.

- The `http.ResponseWriter` interface mandates 3 methods:
    ```go
        package http

        type ResponseWriter interface {
            Header() Header
            Write([]byte) (int error)
            WriteHeader(statusCode int)
        }
    ```
  The general flow of using ResponseWriter instances would be: set headers,
  write the response body, then return.

- `log.Fatal(http.ListenAndServe("localhost:8000", handler))` is a good way
  to kick off the HTTP server.

- `Request.URL.Path` to access the requested URL.

- `Request.URL.Query()` will return a Value instance:
  `type Values map[string][]string`. Values is a map, but more particularly,
  it's called a multimap since the key points to a slice/list of values.
  Among others, Values defines two methods:
    1. `func (v Values) Get(key string) string`
    2. `func (v Values) Set(key, value string)`
  Note: Request body parameters are not included here. For those, use methods
  like `Request.ReadBody`, `Request.ParseForm`, `Request.ParseMultipartForm`,
  and so on. For more info: https://golang.org/pkg/net/http/#Request

- Quick shortcut for returning non-successful HTTP responses:
  `http.Error(w, msg, http.StatusNotFound)`. `http.StatusNotFound` is just a
  constant and it expands to the integer `404`. For more check out
  https://golang.org/pkg/net/http/#pkg-constants.

- a `http.ServeMux` object can be set up and passed to `http.ListenAndServe`
  to make handling the associations between handlers and their URLs cleaner.
  Use `http.ServeMux.Handle(url string, handler http.Handler)` to add handlers
  to the ServeMux. Since ServeMux itself implements the Handler interface, we
  can pass ServeMux instances to other ServeMux instances!

- In the textbook's example, `db` in an object which has two methods: `list`
  and `price`. Both of these methods have the same type signature as what's
  required of a `http.Handler`. But their names are not "Handler" and this db
  does not actually satisfy the `http.Handler` interface. To get around this
  the `http` package offers us a type: `http.HandlerFunc`. With explicit type
  conversions, we can convert any function of the type:
  `func(http.ResponseWriter, *http.Request)` and convert it into something
  that satisfies the `http.Handler` interface.
  The `http.HandlerFunc` function is unique since it's a function that defines
  a receiver method `ServerHttp` in which it calls itself. It's an acceptably
  hacky way of satisfying the `http.Handler` interface and a good example of
  how function types are also a kind of type just like int, float, bool, etc.
  Look at: https://golang.org/pkg/net/http/#HandlerFunc for more info.

- When using a ServeMux, instead of using `.Handle(url, HandlerFunc(...))` we
  have a convenience method with combines the two: `ServeMux.HandleFunc`.

- There's also a global ServeMux instance defined in the `http` package. To
  add handler functions to it, there's also a global method `http.HandleFunc`.
  When we pass nil as the second argument to the `http.ListenAndServe` method,
  this global ServeMux will be used.


### 7.8 The error Interface
- Just need one method:
    ```go
        type error interface {
            Error() string
        }
    ```

- The errors interface is very small:
    ```go
        package errors
        func New(text string) error { return &errorString{text} }
        type errorString struct { text string }
        func (e *errorString) Error() string { return e.text }
    ```

- The `syscall` package has `type Errno uintptr` which implements the `error`
  interface to print out the standard error message for that given error code.


### 7.9 Example: Expression Evaluator
- (nothing new to note here)


### 7.10 Type Assertions
- (For notes on this chapter, refer to `./assertion.go`. This part is something
   better shown, not said - i.e. better something demonstrated not noted.)


### 7.11 Discriminating Errors with Type Assertions
- Using the error message string to determine the type of error is naive and
  inefficient. Instead use custom types which implement the error interface
  then use type assertions.


### 7.12 Querying Behaviors with Interface Type Assertions
- As the name suggests we can use a type assertion to guess the concrete type
  that an interface represents and in doing so we may find more available
  methods for a given object.

- All of `*bytes.Buffer`, `*os.File`, and `*bufio.Writer` all satisfy the
  `io.Writer` interface which writes the input byte-slice. But since writing
  a string is a common activity, they also have a `WriteString` method which
  while is not required by the `io.Writer` interface, provides a way to write
  a string with less overhead.

- To see if a method has a `WriteString` method, simply create a new interface
  for it and attempt an interface-to-interface type assertion. Luckily for us
  though, the `io` package already defines this interface as `io.WriteString`
  so we don't have to re-define it ourselves (though it really wouldn't be
  much work even if we needed to - it would just clutter things up a bit).

- The `fmt.Println` method uses type assertions to check if it's dealing with
  an error or not. For anything satisfying the `error` interface it calls the
  `Error` method otherwise if it satisfies the `Stringer` interface, it will
  call `String` otherwise try something else (using reflection).


### 7.13 Type Switches
- One main way interfaces are used are to assert expected behavior/methods.

- The other common way interfaces are used are to hold a union of types
  (sometimes called *discrimintated unions*).

- *type switch*s are a way to perform some operation on an interface based on
  it's dynamic type using switch statements instead of an if-else chain.

- Example:
    ```go
        switch x.(type) {
        case nil:
        // ...
        case int, uint:
        // ...
        default:
        // ...
        }
    ```

- Yes, that's literally the keyword "type" being used there in the switch
  statement.

- The cases don't necessarily have to be concrete types, they can also be
  interfaces.

- There's also an extended form of the `type switch` in which we can access
  the extracted dynamic type. Example:
    ```go
        switch x := x.(type) {
        case nil:
        // ...
        case int, uint:
        // ...
        default:
        // ...
        }
    ```
  "Here we’ve called the new variables x too; as with type assertions, reuse
  of variable names is common."


### 7.14 Example: Token-Based XML Decoding
- (nothing new to note here)


### 7.15 A Few Words of Advice
- "Interfaces are (generally) only needed when there are two or more concrete
  types that must be dealt with in a uniform way."

- "Small interfaces are easier to satisfy when new types come along."

- "A good rule of thumb for interface design is ask only for what you need."
