package chapter07

import (
	"flag"
	"fmt"
	"strconv"
)

// An extension of the example in "../Introducing Go/chapter08/cli/main.go"
// There, we used the flag package but we never used flag.Value.

// Temperature in Kelvin.
type Temperature float64

func (t *Temperature) String() string {
	return fmt.Sprintf("%.2f", *t)
}

func (t *Temperature) Set(s string) error {
	val_str, unit := s[:len(s)-1], s[len(s)-1]

	val, err := strconv.ParseFloat(val_str, 64)
	if err != nil {
		return fmt.Errorf("Invalid temperature: \"%s\". Must be a real number.", val_str)
	}

	switch unit {
	case 'C':
		*t = Temperature(val + 273.15)
	case 'F':
		*t = Temperature((val-32)*(5/9) + 273.15)
	case 'K':
		*t = Temperature(val)
	default:
		return fmt.Errorf("Invalid unit. Must be either C, F, or K.")
	}

	return nil
}

func DemoFlagValue() {
	t := new(Temperature)
	flag.CommandLine.Var(t, "t", "The temperature followed by unit (either C, F or K). Example: 32C.")
	flag.Parse()
	fmt.Println(t)
}
