package chapter07

import (
	"fmt"
	"io"
	"os"
)

type Test string

func (t *Test) String() string {
	return string(*t)
}

type TestX string

func (t *TestX) String() string {
	return string(*t)
}

func ignore(...interface{}) {
}

func DemoInterfaceToConcreteTypeAssertion() {
	z := Test("Hello")
	y := &z
	x := fmt.Stringer(y)

	/* Successful type assertion. */
	a := x.(*Test)

	/* Incorrect type assertion that won't panic: */
	b, ok := x.(*TestX) /* b, ok = nil (zero value of the asserted type), false */
	fmt.Println(b)

	/* This synatx is often used with the if res, ok := something(); ok { use res } syntax. */

	/* Incorrect type assertion that will panic: */
	c := x.(*TestX) /* Panics. */

	ignore(a, ok, c)
}

func DemoInterfaceToInterfaceTypeAssertion() {
	x := io.Writer(os.Stdout)
	y := x.(io.ReadWriter)
	z := y.(io.Reader) /* Though here we could just use type conversion: io.Reader(y) */
	ignore(z)
}

func DemoTypeAssertionFromNil() {
	/* No matter what, performing a type assertion with nil will result in a panic (or false). */
	/* "No matter what type was asserted, if the operand is a nil interface value, the type
	   assertion fails. A type assertion to a less restrictive interface type (one with fewer
	   methods) is rarely needed, as it behaves just like an assignment, except in the nil case." */
	m := interface{}(nil) /* We can't just do m := nil since that would be an "untyped nil". */
	n := m.(interface{})
	ignore(m, n)
}
