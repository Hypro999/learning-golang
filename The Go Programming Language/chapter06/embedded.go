package chapter06

import "fmt"

type Player struct {
	Person // Defined in simple_demos.go
	Level  int
}

type PlayerV2 struct {
	Person // Defined in simple_demos.go
	Level  int
}

func (p *PlayerV2) String() string {
	return fmt.Sprintf("<player: %s>", p.Name)
}

func DemoEmbeddedStructs() {
	p := Player{Person: Person{Name: "Hypro999"}, Level: 100}
	fmt.Println(p.String())

	p2 := PlayerV2{Person: Person{Name: "Hypro999"}, Level: 100}
	fmt.Println(p2.String())
}
