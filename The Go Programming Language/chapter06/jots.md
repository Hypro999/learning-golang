# Chapter 6 - Methods
Just a few notes here and there. Mostly for stuff that I either haven't seen
before or want to really reiterate.


### 6.1 Method Declarations
- The method call expression `variableName.MethodName` is called a "selector"
  because the there might be another method with the name `MethodName`, but if
  it's not a receiver for the type of `variableName` then it's not a
  re-declaration and thus there would be no issue with it existing. So when
  the compiler sees `MethodName` it will also see the preceding
  `variableName.` and then select the implementation of `MethodName` to use.

- Go does *not* have inheritance. For example, if we defined:
    ```go
        type typeA int
        type typeB typeA
        type typeC typeA

        func (a typeA) Foo() {
            fmt.Println("foo")
        }
    ```
  instances of typeB and typeC won't be able to use the typeA.Foo() method.

- Methods can't be defined for pointers and interfaces but they can be defined
  functions.


### Methods with a Pointer Receiver
- When the receiver function needs to update the calling variable, make it a
  pointer based receiver (e.g. `func (a *typeA) Foo()` and the name of this
  function would be `(* typeA).Foo`). 

- By convention, make either all or none of the receivers for a given type be
  pointer receivers.

- "To avoid ambiguities, method declarations are not permitted on named types
  that themselves are pointer types."

- Look at the example `SimpleMethodDemo` in `simple_demos`. It's pretty
  important.

- Be careful with nil-value pointers since they can still be passed to
  receiver methods and this might lead to segfaults if they weren't
  anticipated.


### 6.3 Composing Types by Struct Embedding
- When we use the syntactic sugar of embedding structs for composition, the
  methods of the embedded struct are "promoted" and can be used directly by
  variables of the embedding struct type. For example:
  ```go
    type ColoredPoint struct {
        Point
        Color color.RGBA
    }
  ```
  Here `ColoredPoint` variables can call receiver methods belonging to `Point`
  directly. If `Point` had a method `foo` and we defined a new method `foo`
  for `ColoredPoint` then when using variables of type `ColoredPoint`, calls
  to `foo` will use the `foo` defined for `ColoredPoint`, else they would use
  the `foo` defined for `Point`.  (See `embedded.go`)

- If two methods with the same name are promoted from the same level or rank
  this way then the compiler would throw an error. For example:
  ```go
    type ColoredPoint struct {
        Point
        color.RGBA
    }
  ```
  Here, if both Point and color.RGBA had methods with the same name, and this
  method was not explicitly masked by (rewritten for) `ColoredPoint` then
  having a variable of type `ColoredPoint` try to call this method would
  result in a compile-time error.


### 6.4 Method Values and Expressions
- You don't have to select and call a method at the same time. Selectors are
  like references to a function and this you can store a selector in a
  variable and then call it later. So similar to how a function can be stored
  as a function value, a method can be stored as a *method value*.

- A *method expression* is something pretty similar to a method value. If a
  method value stores the method for an instance of a given type, a method
  expression will store the method for the given type itself, letting the
  first argument of the method now be an instance of the given type itself and
  all other arguments' positions would have been shifted over by one.


### 6.5 Example: Bit Vector Type
- (nothing new to note here)


### 6.6 Encapsulation
- (nothing new to note here)

