package chapter06

import "fmt"

type Point struct {
	X int
	Y int
}

// While it's not strictly needed, this method is a pointer receiver method
// instead of being a regular receiver method.
func (p *Point) String() string {
	return fmt.Sprintf("(%d, %d)", p.X, p.Y)
}

type Person struct {
	Name string
}

func (p Person) String() string {
	return fmt.Sprintf("<Name: %s>", p.Name)
}

func SimpleMethodDemo() {
	a := Point{1, 1}
	b := &Point{2, 2}
	c := &a

	fmt.Printf("%v %v %v %v\n", a, a.String(), b, c)

	x := Person{"Alice"}
	y := Person{"Bob"}
	z := &x

	fmt.Printf("%v %v %v %v\n", x, y, z, z.String())

	// Would lead to a segfault:
	// var p *Point
	// fmt.Printf(p.String())
}
