package chapter06

import "fmt"

func DemoMethodValuesAndExpressions() {
	p := Person{"Steve"}
	pString := p.String           // method value.
	personString := Person.String // method expression.
	fmt.Println(p.String())
	fmt.Println(pString())
	fmt.Println(personString(p))
}
