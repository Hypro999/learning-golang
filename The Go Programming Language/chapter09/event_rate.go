package chapter09

// Exercise 9.5

import (
	"fmt"
	"runtime"
	"time"
)

func DemoRoundTripCommunicationRate() {
	a := make(chan interface{})
	b := make(chan interface{})
	c := make(chan interface{})

	go func() {
		for {
			b <- nil
			<-a
			c <- nil
		}
	}()

	go func() {
		for {
			<-b
			a <- nil
		}
	}()

	cnt := 0 // Only modifyable from the main goroutine (for this function).
	start := time.Now()
	for {
		<-c
		cnt += 1
		if cnt%1000000 == 0 {
			mps := float64(cnt) / time.Since(start).Seconds()
			go fmt.Printf("\r%f round-trip events per second.", mps)
			cnt = 0
			start = time.Now()
		}
	}
}

func DemoRawEventRate() {
	grs := runtime.GOMAXPROCS(0) - 1
	ch := make(chan interface{})

	for i := 0; i < grs; i++ {
		go func() {
			for {
				ch <- nil
			}
		}()
	}

	cnt := 0 // Only modifyable from the main goroutine (for this function).
	start := time.Now()
	for {
		<-ch
		cnt += 1
		if cnt%1000000 == 0 {
			mps := float64(cnt) / time.Since(start).Seconds()
			go fmt.Printf("\r%f events per second.", mps)
			cnt = 0
			start = time.Now()
		}
	}
}
