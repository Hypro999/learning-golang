# Chapter 9 - Concurrency with Shared Variables
Just a few notes here and there. Mostly for stuff that I either haven't seen
before or want to really reiterate.


### 9.1 Race Conditions
- Common types of race conditions:
    1. Data Race - when two or more goroutines try to access the same variable and at least one of
   those accesses is a write access.

    2. undefined behaviour - Dangerous race conditions which can lead to memory corruption or other
   severe problems (usually something C programmers have to constantly worry about).

- Common ways to avoid race conditions:
    1. Avoid writing: if possible, pre-populate every shared variables then restrict each goroutine
   to read-only access of shared variables.

    2. Use monitor goroutines: confine variables to a single goroutine then apply the "don't
   communicate by sharing memory; instead, share memory by communicating." approach.

    3. Serial confinement: in pipelines, once a variable has been passed along, never access it
   again from that goroutine.

    4. Mutual exclusion: use stuff like mutex-locks to restrict memory access.


### 9.2 Mutual Exclusion: sync.Mutex
- Mutexes are supported in golang with the `sync.Mutex` type. It defines two notable methods:
  `Lock()` and `Unlock()`.

- "By convention, the variables guarded by a mutex are declared immediately after the declaration
  of the mutex itself. If you deviate from this, be sure to document it."

- A good way to enforce lock usage is to exploit package-level access control. Make shared
  variables unexported and thus manipulatable only through exported methods (which will use
  locks). This combined arrangement of functions, locks and variables is called a "monitor"
  (similar to the same concept in Java).

- It's a good idea to put the entire critical section in a single function and defer lock
  release so that we never forget to unlock a mutex by accident. Plus this way we only have
  to write one unlock statement.

- Refer to the deposit/withdraw example in the textbook. Where needed, it suggests making two
  versions of a function (usually one that is exported and one that is unexported) where one
  method employs locking while the other is designed to be used when something else has already
  locked the variable (when the critical section of one method is a subset of another).
  E.g. `Withdraw` and `Deposit` both define their own locking and unlocking statements and
  internally call `rawModifyBalance` which defines no locking and unlocking statements for itself.

- In a nutshell, encapsulation + locking can lead to some pretty good concurrency control.


### 9.3 Read/Write Mutexes: sync.RWMutex
- The "multiple readers, single writer lock". Similar to that concept in DBMS - "shared mode lock".
  From this perspective, `sync.Mutex` would be an "exclusive mode lock".

- Now the methods are `RLock()` and `RUnlock()`.

- "It’s only profitable to use an RWMutex when most of the goroutines that acquire the lock are
  readers, and the lock is under contention, that is, goroutines routinely have to wait to acquire
  it. An RWMutex requires more complex internal bookkeeping, making it slower than a regular
  mutex for uncontended locks."


### 9.4 Memory Synchronization
- "Synchronization primitives like channel communications and mutex operations cause the processor
  to flush out and commit all its accumulated writes (from the cache) so that the effects of
  goroutine execution up to that point are guaranteed to be visible to goroutines running on other
  processors."

- The example given in the book must be theoretically possible but practically very rare.

- Check out: https://golang.org/ref/mem

- Regarding stuff like compiler optimizations and CPU cache/RAM related issues:
  "All these concurrency problems can be avoided by the consistent use of simple, established
  patterns. Where possible, confine variables to a single goroutine; for all other variables,
  use mutual exclusion."
  Basically, don't try to reason about the execution of a program in a concurrent environment
  becuase chances are more often than not we'll reason about the execution incorrectly (most
  of the times because we won't know what compiler optimizations will be used or how the CPU
  handles the execution of instructions). Instead stick to identifying established patterns
  and then applying the conventional fix.


### 9.5 Lazy Initialization: sync.Once
- When a mutex is only supposed to be used once during program execution and then never again,
  then sync.Once will be useful. Example usage: lazy-loading a global map variable that won't
  be modified after it is loaded for the first time.

- The more complex alternative would be to first use a shared lock to see if initialization has
  taken place. Then to release the shared lock and obtain an exculsive lock if initialization is
  needed. But this leads to twice the number of critical sections.

- Only method: `Do()` which takes a method to execute and makes sure that it's executed only once
  per run of the entire program. Use an anonymous function if you need to call a method with
  specific arguments.


### 9.6 The Race Detector
- Dynamic analysis tool.

- A `-race` flag can be used with `go build`, `go run`, and `go test`.


### 9.7 Concurrent Non-blocking Cache
(A good example but nothing to really put down in written notes.)


### 9.8 Goroutines and Threads
- OS threads usually have a fixed size stack area - e.g. 2 MB. With goroutines, stacks start with
  a smaller initial value (e.g. 1 KB) then they are dynamically resized as needed.

- OS threads are scheduled by the OS kernal. The context switch between OS threads is a "full
  context switch" (one thread's state has to be saved to memory, then another thread's state has
  to be restored, and then the scheduler's data structures have to be updated). This has low
  locality (i.e. branch prediction and stuff get messed up) and has quite a fair number of memory
  accesses.

- Goroutines are managed by in part by the Go runtime's scheduler. The technique it uses is "m:n
  scheduling" where m goroutines are multiplexed/scheduled over n OS threads.

- OS scheduler: Triggered by hardware interrupts.
  Go scheduler: Done implicitly by certain language constructs (compile time?). 

- "The Go scheduler uses a parameter called GOMAXPROCS to determine how many OS threads may be
  actively executing Go code simultaneously. Its default value is the number of CPUs on the
  machine"

- "You can explicitly control this parameter using the GOMAXPROCS environment variable or the
  runtime.GOMAXPROCS function"

- Goroutines have no unique identities. Usually OS threads have some kind of unique identifier but
  goroutines don't. This means that stuff-like "thread-local storage" are not possible. This was
  an explicit design decision.
