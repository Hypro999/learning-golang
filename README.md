# Learning Go
This repository will serve as a central repository of all of the sample code (snippets, examples, mini-projects, etc.)
that I will create or come across and want to save as I learn Go from all kinds of different places.

Books Completed:
- [Introducing Go](https://g.co/kgs/skKn2k)
- [The Go Programming Language](https://www.gopl.io/)

I read articles fairly quickly and don't follow much of a methodology here.
So I'm not tracking/saving links for articles I read - just the knowledge.
