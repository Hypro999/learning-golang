### (Meta) Notes:
- I didn't save any of the examples or notes I wrote for the first few chapters:
1. An Introduction to Concurrency
2. Modeling Your Code: Communicating Sequential Processes
3. Go's Concurrency Building Blocks

Because I've already covered most of this stuff in the past in various ways. The
idea of concurrency v/s parallelism was covered in Rob Pike's talk of the same
title. These concepts were also covered in the Operating Systems and Parallel
Programming courses I took at uni. The idea of race conditions is no alien to me
either (see https://github.com/Hypro999/racy-django).

Since pthreads has a really similar set of primitives for sharing information by
sharing memory (threads, mutexes, conditional variables) I only had to double
check if the behaviour in Go was the same and where these primitives lived.

Goroutines, channels, and select statements were already covered EXTENSIVELY in
the previous two books.

Making and maintaining notes that I would have been happy with would have been a
huge pain so I'm just leaving it at this meta-note.

