package chapter04

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

// We use the "preventing goroutine leaks" pattern here when defining pipeline
// stage functions.
func AddIntStream(done <-chan struct{}, in <-chan int, amt int) <-chan int {
	out := make(chan int, 3)
	go func() {
		defer close(out)
		for x := range in {
			select {
			case <-done:
				return
			case out <- x + amt:
			}
		}
	}()
	return out
}

func MultiplyIntStream(done <-chan struct{}, in <-chan int, mltp int) <-chan int {
	out := make(chan int, 3)
	go func() {
		defer close(out)
		for x := range in {
			select {
			case <-done:
				return
			case out <- x * mltp:
			}
		}

	}()
	return out
}

// Why can't chan int be used for chan interface{}? :(
func IntToInterfaceStream(done <-chan struct{}, in <-chan int) <-chan interface{} {
	out := make(chan interface{}, 3)
	go func() {
		defer close(out)
		for x := range in {
			select {
			case <-done:
				return
			case out <- x:
			}
		}
	}()
	return out
}

func toFloat(x interface{}) (float64, error) {
	switch y := x.(type) {
	case int:
		return float64(y), nil
	case float32:
		return float64(y), nil
	case float64:
		return y, nil
	case string:
		f, err := strconv.ParseFloat(y, 64)
		if err != nil {
			return 0, err
		}
		return f, nil
	default:
		return 0, fmt.Errorf("unexpected datatype")
	}
}

func ToFloatStream(done <-chan struct{}, in <-chan interface{}) <-chan float64 {
	out := make(chan float64, 3)
	go func() {
		defer close(out)
	l1:
		for x := range in {
			y, err := toFloat(x)
			select {
			case <-done:
				return
			default:
				if err != nil {
					continue l1
				}
			}
			select {
			case <-done:
				return
			case out <- y:
			}
		}
	}()
	return out
}

func getRandomInt() int {
	return rand.Intn(10)
}

func RandIntStream(done <-chan struct{}) <-chan int {
	out := make(chan int)
	go func() {
		defer close(out)
		for {
			select {
			case <-done:
				return
			case out <- getRandomInt():
			}
		}
	}()
	return out
}

func TakeInt(done <-chan struct{}, in <-chan int, n int) <-chan int {
	out := make(chan int)
	go func() {
		i := 0
		defer close(out)
		if n < 1 {
			return
		}
		for x := range in {
			select {
			case <-done:
				return
			case out <- x:
				i += 1
			}
			if i == n {
				return
			}
		}
	}()
	return out
}

func LargePipelineDemo() {
	done := make(chan struct{})
	defer close(done)

	resultsStream := MultiplyIntStream(done, AddIntStream(done, TakeInt(done, RandIntStream(done), 10), 1), 2)
	floatResultsStream := ToFloatStream(done, IntToInterfaceStream(done, resultsStream))
	timeoutChan := time.After(time.Nanosecond * time.Duration(rand.Intn(1000000)+1000000)) // 1ms - 2ms
l1:
	for {
		select {
		case <-timeoutChan:
			fmt.Println("timeout.")
			break l1
		case result, ok := <-floatResultsStream:
			if !ok {
				fmt.Println("closed.")
				break l1
			}
			fmt.Printf("%T: %.2f\n", result, result)
		}
	}
}
