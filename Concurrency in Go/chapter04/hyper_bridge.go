package chapter04

import (
	"fmt"
	"sync"
	"time"
)

// This turns the input slice of streams into a stream of streams.
// In many ways, this new stream is like a queue that we're unloading.
func streamStreams(done <-chan struct{}, streams []chan interface{}) <-chan chan interface{} {
	streamOfStreams := make(chan chan interface{})
	go func() {
		defer close(streamOfStreams)
		for _, stream := range streams {
			select {
			case <-done:
				return
			case streamOfStreams <- stream:
			}
		}
	}()
	return streamOfStreams
}

func orDone1(done <-chan struct{}, in <-chan chan interface{}) <-chan chan interface{} {
	out := make(chan chan interface{})
	go func() {
		defer close(out)
		for {
			select {
			case <-done:
				return
			case msg, ok := <-in:
				if !ok {
					break
				}
				select {
				case <-done:
					return
				case out <- msg:
				}
			}
		}
	}()
	return out
}

// A more parallel version of the bridge pattern given by Katherine Cox-Buday
// in "Concurrency Patterns in Go".
func hyperBridge(done <-chan struct{}, streamOfStreams <-chan chan interface{}) <-chan interface{} {
	confluence := make(chan interface{})
	go func() {
		defer close(confluence)
		var wg sync.WaitGroup
		for stream := range orDone1(done, streamOfStreams) {
			fmt.Printf("Got new stream: %v\n", stream)
			wg.Add(1)
			go func(stream <-chan interface{}) {
				defer wg.Done()
				for {
					select {
					case <-done:
						return
					case val, streamOpen := <-stream:
						if !streamOpen {
							return
						}
						select {
						case <-done:
							return
						case confluence <- val:
						}
					}
				}
			}(stream)
		}
		fmt.Println("Now just waiting...")
		wg.Wait()
	}()
	return confluence
}

// Combine the bridge pattern with fan-in.
// Good intro to the idea of stream-of-streams,
// I.E. A channel which returns another channel.
func HyperBridgeDemo() {
	done := make(chan struct{})
	defer close(done)

	streams := make([]chan interface{}, 3)
	for i := 0; i < len(streams); i++ {
		streams[i] = make(chan interface{})
		go func(i int, stream chan<- interface{}) {
			defer close(stream)
			for {
				select {
				case <-done:
					return
				case stream <- i:
				}
			}
		}(i, streams[i])
	}

	timer := time.After(time.Second * time.Duration(5))
	confluence := hyperBridge(done, streamStreams(done, streams))

	for {
		select {
		case <-timer:
			return
		case val, streamOpen := <-confluence:
			if !streamOpen {
				return
			}
			fmt.Println(val)
		}
	}
}
