package chapter04

import (
	"fmt"
	"time"
)

func generator(done <-chan struct{}) <-chan interface{} {
	out := make(chan interface{})
	go func() {
		defer close(out)
		x := 0
		for {
			select {
			case <-done:
				return
			case out <- x:
				x++
			}
		}
	}()
	return out
}

func orDone(done <-chan struct{}, in <-chan interface{}) <-chan interface{} {
	out := make(chan interface{})
	go func() {
		defer close(out)
		for {
			select {
			case <-done:
				return
			case msg, ok := <-in:
				if !ok {
					break
				}
				select {
				case <-done:
					return
				case out <- msg:
				}
			}
		}
	}()
	return out
}

func printer(done <-chan struct{}, in <-chan interface{}) <-chan struct{} {
	out := make(chan struct{})
	go func() {
		defer close(out)
		for msg := range orDone(done, in) {
			fmt.Println(msg)
		}
	}()
	return out
}

func OrDoneDemo() {
	done := make(chan struct{})

	input := generator(done)
	output := printer(done, input)

	go func() {
		<-time.After(time.Second * time.Duration(1))
		close(done)
	}()

	<-output // Thread join
}
