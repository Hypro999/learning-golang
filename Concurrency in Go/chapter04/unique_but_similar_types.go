package chapter04

import (
	"fmt"
)

type customT int

func TypedefDemo() {
	myMap := map[interface{}]string{}
	myMap[customT(1)] = "alpha"
	myMap[1] = "beta"
	fmt.Println(myMap) // {1: "alpha", 1: "beta"}
}
