package chapter04

/*
 * This example also indirectly covers the techinque used by fan-in
 * I.E. multiplexing the input by creating 1 go routine per channel
 * to be multiplexed and using waitgroups.
 */

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// [BREAKS CONVENTION] This pipeline stage will return the input stream
// to it whereas usually pipeline stages return their output stream.
// This stage does not have an output stream.
func makePrintStream(done <-chan struct{}) chan<- interface{} {
	stdout := make(chan interface{})
	go func() {
		for {
			select {
			case <-done:
				return
			case msg, ok := <-stdout:
				if !ok {
					return
				}
				select {
				case <-done:
					return
				default:
					fmt.Println(msg)
				}
			}
		}
	}()
	return stdout
}

func makeInputStream(done <-chan struct{}) <-chan interface{} {
	out := make(chan interface{})
	go func() {
		defer close(out)
		for i := 0; i < 10; i++ {
			x := rand.Intn(10)
			select {
			case <-done:
				return
			case out <- x:
			}
		}
	}()
	return out
}

func makeLogStream(done <-chan struct{}, in <-chan interface{},
	printStream chan<- interface{}, prefix string) <-chan struct{} {
	exiting := make(chan struct{})
	go func() {
		defer close(exiting)
		for {
			select {
			case <-done:
				return
			case msg, ok := <-in:
				if !ok {
					return
				}
				msg = fmt.Sprintf("%s: %v", prefix, msg)
				select {
				case <-done:
					return
				case printStream <- msg:
				}
			}
		}
	}()
	return exiting
}

func tee(done <-chan struct{}, in <-chan interface{}) (<-chan interface{}, <-chan interface{}) {
	out1 := make(chan interface{})
	out2 := make(chan interface{})
	go func() {
		defer close(out1)
		defer close(out2)
		for msg := range in {
			out1, out2 := out1, out2
			for i := 0; i < 2; i++ {
				select {
				case <-done:
					return
				case out1 <- msg:
					out1 = nil
				case out2 <- msg:
					out2 = nil
				}
			}
		}
	}()
	return out1, out2
}

// Use the fan-in pattern to wait for all of the end channels
// to signal that they are done.
func waitAll(done <-chan struct{}, end ...<-chan struct{}) <-chan struct{} {
	exiting := make(chan struct{})
	go func() {
		defer close(exiting)
		var wg sync.WaitGroup
		wg.Add(len(end))
		for _, ch := range end {
			go func(ch <-chan struct{}) {
				defer wg.Done()
				select {
				case <-done:
				case <-ch:
				}
			}(ch)
		}
		wg.Wait()
	}()
	return exiting
}

func TeeDemo() {
	done := make(chan struct{})
	start := time.Now()
	defer func() {
		close(done)
		fmt.Printf("CPU time taken: %v\n", time.Since(start))
	}()

	stdout := makePrintStream(done)
	in1, in2 := tee(done, makeInputStream(done))
	wait := waitAll(
		done,
		makeLogStream(done, in1, stdout, "first"),
		makeLogStream(done, in2, stdout, "second"),
	)
	timer := time.After(time.Second * time.Duration(1))
	select {
	case <-wait:
		fmt.Println("Completed")
	case <-timer:
		fmt.Println("Timeout")
	}
}
