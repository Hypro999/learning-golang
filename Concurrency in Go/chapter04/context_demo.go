package chapter04

import (
	"context"
	"fmt"
	"time"
)

// Prevent conflict with other packages using strings for keys with the same context object.
// In this case this is premature optimization (read: overkill), but this is a demo so chuck it.
type keyT string

func foo(ctx context.Context) <-chan string {
	spam := make(chan string)

	go func() {
		defer close(spam)
		for {
			select {
			case _, open := <-ctx.Done():
				if !open {
					return
				}
			case spam <- "hi":
			}
		}
	}()
	return spam
}

func ContextDemo() {
	// Note: In actual code, we would probably pass this "agent" value as an actual parameter
	// to the function that is using it. It would lead to better readability.
	ctx, cancel := context.WithTimeout(context.WithValue(context.Background(), keyT("agent"), "viper"), time.Second*2)
	defer cancel()

	start := time.Now()
	for val := range foo(ctx) {
		fmt.Println(val)
	}
	fmt.Printf("Time elapsed: %v\n", time.Since(start))
}
