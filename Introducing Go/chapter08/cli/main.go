// A demonstration for parsing command line arguments to make a command line interface (CLI)
// The flags package makes it pretty easy. The only downside is that we can use the standard
// getopt(3) style of command line arugments (e.g. supporting both -v and --verbose)

package main

import (
	"flag"
	"fmt"
	"os"
)

func main() {
	var parallel int
	var host string

	flag.IntVar(&parallel, "parallel", 1, "The number of parallel goroutines to run.")
	flag.StringVar(&host, "host", "localhost", "The host you want to target.")
	rawPort := flag.Int("port", 80, "The port on the host you want to target.")

	flag.Parse()

	if *rawPort <= 0 || *rawPort > 65535 {
		fmt.Println("Invalid port number.")
		os.Exit(2)
	}
	port := uint16(*rawPort)

	fmt.Println(parallel)
	fmt.Println(host)
	fmt.Println(port)
}
