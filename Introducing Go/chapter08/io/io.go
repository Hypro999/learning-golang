package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func die(err error) {
	fmt.Println(err)
	os.Exit(-1)
}

func checkErr(err error) {
	if err != nil {
		die(err)
	}
}

// Open the specified file, exit if we can't otherwise
// stat it, print it's size and return it.
func openAndStatFile(filename string) *os.File {
	file, err := os.Open("sample.txt")
	checkErr(err)
	stats, err := file.Stat()
	checkErr(err)
	fmt.Printf("File has %d bytes.\nContents:\n", stats.Size())
	return file
}

// Read from a file into a primitive buffer (just a byte slice) 256 bytes
// at a time. Buffered IO > reading everything into memory at once.
func bufferedFileReadingDemo() {
	file := openAndStatFile("../sample.txt")
	defer func() {
		fmt.Println("Closing file.")
		file.Close()
	}()

	rd := -1
	buffer := make([]byte, 256)
	for rd != 0 {
		rd, err := file.Read(buffer)
		if err != nil {
			if err.Error() == "EOF" {
				return
			}
			die(err)
		}
		fmt.Print(string(buffer[:rd]))
	}
}

// Read from a file using ioutils.
func ioutilsFileReadingDemo() {
	buf, err := ioutil.ReadFile("sample.txt")
	checkErr(err)
	fmt.Println(string(buf))
}

// Execute with go run io/io.go for the correct working directory.
func main() {
	bufferedFileReadingDemo()
	ioutilsFileReadingDemo()
}
