// A demonstration of writing to and reading from a CSV file.

package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/google/uuid"
)

// SAVEFILE is the name of the file which this program
// will store data to and load data from.
const SAVEFILE = "players_register.csv"

const (
	// AS : Asia.
	AS uint8 = 1

	// US : The United States.
	US uint8 = 2

	// UK : The United Kingdom.
	UK uint8 = 3
)

// Player : A player on xyz's server.
type Player struct {
	id     uuid.UUID
	name   string
	region uint8
}

// String returns the string representation of a Player.
func (player *Player) String() string {
	return fmt.Sprintf("%v,%s,%d", player.id, player.name, player.region)
}

// Bytes returns a bytearray of the string representation of a Player.
func (player *Player) Bytes() []byte {
	return []byte(player.String())
}

// Register is a slice of players.
type Register []Player

// Save a register of players to a CSV file.
// We use plain vanilla IO (Basically OS syscalls) to achieve this.
// Using buffered IO would be better, but we won't just for the
// sake of demonstration of these basic IO syscalls.
func (reg Register) Save() error {
	fmt.Print("Saving players register to disk... ")
	defer fmt.Println("Done!")

	file, err := os.OpenFile(SAVEFILE, os.O_RDWR|os.O_CREATE, 0660)
	if err != nil {
		return err
	}
	defer file.Close()

	for _, player := range reg {
		data := player.Bytes()
		data = append(data, []byte("\n")...)
		_, err := file.Write(data)
		if err != nil {
			return err
		}
	}

	return nil
}

// LoadRegister will load and return a Register from the savefile.
// This time, instead of relying on plain, low-level syscalls for
// IO, we will used buffered IO as enabled via. the bufio package.
func LoadRegister() (Register, error) {
	fmt.Print("Loading players register from disk... ")
	defer fmt.Println("Done!")

	var reg Register
	file, err := os.Open(SAVEFILE)
	if err != nil {
		return nil, err
	}
	bufferedFile := bufio.NewReader(file)

	for {
		rawLine, _, err := bufferedFile.ReadLine()
		if err != nil {
			if err.Error() == "EOF" {
				break
			}
			return nil, err
		}
		line := string(rawLine)
		splitLine := strings.Split(line, ",")

		// Convert the UUID string back into a UUID object.
		id, err := uuid.Parse(splitLine[0])
		if err != nil {
			return nil, err
		}

		// Parse the region into a uint8.
		region, err := strconv.ParseInt(splitLine[2], 10, 8)
		if err != nil {
			return nil, err
		}

		// Create a new player and add it to the Register.
		player := Player{
			id:     id,
			name:   splitLine[1],
			region: uint8(region),
		}
		reg = append(reg, player)
	}

	return reg, nil
}

func main() {
	players := Register{
		{
			id:     uuid.New(),
			name:   "Okarin91",
			region: AS,
		},
		{
			id:     uuid.New(),
			name:   "Dream",
			region: US,
		},
		{
			id:     uuid.New(),
			name:   "George404",
			region: UK,
		},
	}

	if err := players.Save(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	loadedPlayers, err := LoadRegister()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for _, player := range loadedPlayers {
		fmt.Println(player.String())
	}
}
