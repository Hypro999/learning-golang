package main

import (
	"fmt"
	"strings"
)

func main() {
	var exists bool

	// Sort of like `if "fox" in "the quick brown fox"` in Python.
	exists = strings.Contains("The quick brown fox", "fox")
	fmt.Printf("%t\n", exists)

	// Like string.endswith() in Python.
	exists = strings.HasPrefix("pull_request_opened", "pull_request")
	fmt.Printf("%t\n", exists)

	// Like string.startswith() in Python.
	exists = strings.HasSuffix("issue_closed", "closed")
	fmt.Printf("%t\n", exists)

	// Demonstration of split() and join(). The Join method in Go
	// is more intuitive than in Python, and consistant with the
	// Split method.
	message := "Hello there everyone"
	words := strings.Split(message, " ")
	for i, word := range words {
		words[i] = strings.ToLower(word)
	}
	message = strings.Join(words, " ")
	fmt.Println(message)

	// A good way to show breakpoints in lengthy logs:
	fmt.Println(strings.Repeat("A", 32))
}
