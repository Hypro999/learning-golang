package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"
)

// ServerAddr is the address the host:port address the server will be listening on.
const ServerAddr = "0.0.0.0:8080"

// RFC3339 is the timestamp layout followed by RFC 3339.
const RFC3339 = "2006-01-02T15:04:05Z07:00"

var urls = map[string]func(http.ResponseWriter, *http.Request){
	"/": rootView,
}

func rootView(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("%s %s %s %s", time.Now().Format(RFC3339), r.UserAgent(), r.Method, r.URL)
	w.Header().Set("Content-Type", "application/json")
	b, err := json.Marshal(map[string]string{"message": "hello"})
	if err != nil {
		// Send an error 500. Not sure how to do that yet.
		return
	}
	w.Write(b)
}

func startWebServer() {
	fmt.Printf("Launching webserver on %s\n", ServerAddr)
	for endpoint, handler := range urls {
		http.HandleFunc(endpoint, handler)
	}
	err := http.ListenAndServe(ServerAddr, nil)
	if err != nil {
		fmt.Println("Failed to start server.")
		os.Exit(1)
	}
}

func main() {
	startWebServer()
}
