package main

import (
	"fmt"
	"io"
	"net"
	"os"
	"strings"
	"sync"
	"time"
)

// ServerCapacity is the maximum number of connections the server will
// simultaneously handle.
const ServerCapacity uint8 = 2

// SimultaneousClients is The number of clients to emulate in the runClientBarrage test.
const SimultaneousClients = 6

// ServerAddr is the address the server will run on (host:port).
const ServerAddr = "localhost:8080"

// I'm using this as an imperfect semaphore (not really a semphore though)
var serverReady = false

func dieOnError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func handleClientConnection(clientSocket net.Conn, activeConnections *uint8) {
	for {
		buffer := make([]byte, 32)
		bytesRead, err := clientSocket.Read(buffer)
		if err == io.EOF {
			fmt.Println("Client has quit.")
			*activeConnections = *activeConnections - 1
			break
		} else {
			dieOnError(err)
		}
		if bytesRead == 0 {
			continue
		}
		message := string(buffer)
		message = strings.Trim(message, " \t\n\x00")
		fmt.Println(message)
	}
}

func spinUpServer() {
	fmt.Printf("Spinning up server on %s... ", ServerAddr)
	var wg sync.WaitGroup
	activeConnections := uint8(0)
	passiveSocket, err := net.Listen("tcp", ServerAddr)
	dieOnError(err)
	fmt.Println("Done!")
	serverReady = true
	for {
		if activeConnections < ServerCapacity {
			clientSocket, err := passiveSocket.Accept()
			dieOnError(err)
			activeConnections++
			fmt.Printf("New client connection (%d/%d): %s\n", activeConnections, ServerCapacity, clientSocket.RemoteAddr())

			// Launch a goroutine to handle the client connection.
			// IO Multiplexing methods like select() or poll() are used when we need a single threaded server.
			// But now with Golang, multithreading/concurrency is easy. Instead of forking this process or
			// manually creating a new thread we just tell go to run a new goroutine.
			wg.Add(1)
			go handleClientConnection(clientSocket, &activeConnections)
		} else {
			time.Sleep(time.Second)
		}
	}
}

func runClient(i int) {
	conn, err := net.Dial("tcp", ServerAddr)
	dieOnError(err)
	for j := 0; j < 5; j++ {
		message := fmt.Sprintf("Message %d from client %d", j+1, i+1)
		conn.Write([]byte(message))
		// Without this sleep call, the logs will get really garbled.
		// Until I learn about channels, I won't be able to control
		// locking the terminal when writing. Go doesn't have a
		// native semaphore construct sadly, though apparently there's
		// not need for it with channels.
		// Even with this call they get garbled after the first round.
		time.Sleep(time.Second)
	}
	conn.Close()
}

func runClientBarrage() {
	for serverReady == false {
		time.Sleep(time.Second)
	}

	var wg sync.WaitGroup
	wg.Add(SimultaneousClients)
	for i := 0; i < SimultaneousClients; i++ {
		go runClient(i)
	}
	wg.Wait()
	fmt.Println("Done running the client barrage.")
}

func main() {
	var wg sync.WaitGroup
	wg.Add(2)
	go spinUpServer()
	go runClientBarrage()
	wg.Wait()
}
