// Goroutines were already demo'd in chapter08/servers/tcp so here we'll just demo Channels.
// Channels are sort of like pipes/fifos (sort of).

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// HomePageSize is basically serves as a named tuple of
// URL and the size of the response from that URL.
type HomePageSize struct {
	URL  string
	Size int
}

// This sample code here was more or less directly copied from the textbook since it's a really
// good demonstration of using channels but also of the proper pattern to follow when launching
// goroutines in a loop - make the function take an input parameter so that the parameter
// is not changed with each iteration of the loop (because we passed a copy by value during
// the function call). If we didn't do this, because goroutines share the same process space
// (only introducing a separate stack space for each goroutine) the `url` variable would have
// been overwritten.
func demoChannels() {
	urls := []string{
		"http://www.apple.com",
		"http://www.amazon.com",
		"http://www.google.com",
		"http://www.microsoft.com",
		"http://github.com",
		"http://golang.org",
	}

	results := make(chan HomePageSize)

	for _, url := range urls {
		go func(url string) {
			res, err := http.Get(url)
			if err != nil {
				panic(err)
			}
			defer res.Body.Close()
			bs, err := ioutil.ReadAll(res.Body)
			if err != nil {
				panic(err)
			}
			results <- HomePageSize{
				URL:  url,
				Size: len(bs),
			}
		}(url)
	}

	var biggest HomePageSize

	for range urls {
		result := <-results
		if result.Size > biggest.Size {
			biggest = result
		}
	}
	fmt.Println("The biggest home page:", biggest.URL)
}

// Basically demoChannels but without channels. There is a lot of common code
// between the two but I don't want to extract them to separate functions because
// I want to preserve the example `demoChannels` as it is.
// This version does not use channels but has some other (smaller) optimizations
// like avoiding an anonymous function call and avoiding having to create and send
// new structures across a channel. These shouldn't be anywhere near enough to
// offset the advantage of loading multiple webpages in parallel using channels.
func demoLackOfChannels() {
	urls := []string{
		"http://www.apple.com",
		"http://www.amazon.com",
		"http://www.google.com",
		"http://www.microsoft.com",
		"http://github.com",
		"http://golang.org",
	}

	var biggest HomePageSize

	for _, url := range urls {
		res, err := http.Get(url)
		if err != nil {
			panic(err)
		}
		bs, err := ioutil.ReadAll(res.Body)
		if err != nil {
			panic(err)
		}
		if len(bs) > biggest.Size {
			biggest.URL = url
			biggest.Size = len(bs)
		}
		res.Body.Close()
	}

	fmt.Println("The biggest home page:", biggest.URL)
}

func profile(foo func()) {
	start := time.Now()
	foo()
	end := time.Now()
	elapsedTime := end.Sub(start)
	fmt.Println(elapsedTime)
}

func demoBufferedChannels() {
	routines := 5
	bufc := make(chan int, 3)
	for i := 0; i < routines; i++ {
		go func(j int) {
			bufc <- j
			fmt.Printf("Goroutine %d is exiting.\n", j)
		}(i)
	}
	for i := 0; i < routines; i++ {
		time.Sleep(time.Second * time.Duration(3))
		_ = <-bufc
	}
	close(bufc)
}

// Demo code basically directly copied from the textbook (new: added sync instead
// of Scanln and limited the loop to a few iterations).
// Bascially like Unix's select syscall but with the syntax of a switch statement.
func demoSelectOverChannels() {
	c1 := make(chan string)
	c2 := make(chan string)
	go func() {
		for i := 0; i < 2; i++ {
			c1 <- "from 1"
			time.Sleep(time.Second * 2)
		}
	}()
	go func() {
		for i := 0; i < 2; i++ {
			c2 <- "from 2"
			time.Sleep(time.Second * 3)
		}
	}()

	i := 0
	for {
		select {
		case msg1 := <-c1:
			fmt.Println(msg1)
			i++
		case msg2 := <-c2:
			fmt.Println(msg2)
			i++
		case <-time.After(time.Second):
			fmt.Println("timeout")
		default:
			fmt.Println("nothing ready")
		}

		if i >= 4 {
			break
		}
	}
}

// Uncomment what ever demo you want to run
func main() {
	// profile(demoChannels)
	// profile(demoLackOfChannels)

	// demoBufferedChannels()

	// demoSelectOverChannels()
}
