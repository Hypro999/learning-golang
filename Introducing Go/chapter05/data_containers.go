package main

import (
	"fmt"
	"sort"
)

func arrAvg(arr [5]int) float64 {
	var sum int
	for i := 0; i < len(arr); i++ {
		sum += arr[i]
	}
	return float64(sum / len(arr))
}

func demoArrays() {
	fmt.Println(arrAvg([5]int{1, 2, 3, 4, 5}))
}

func sliceAvg(arr []int) float64 {
	var sum int
	for i := 0; i < len(arr); i++ {
		sum += arr[i]
	}
	return float64(sum / len(arr))
}

func demoSlices() {
	fmt.Println("Avg:", sliceAvg([]int{1, 2, 3, 4, 5}))

	// If we make then append we will append after the reserved space,
	// just like with C++ vectors. Here the 5 represents the slice size.
	x := make([]int, 5)
	for i := 5; i < 10; i++ {
		x = append(x, i)
	}
	fmt.Println("x:", x)
	fmt.Println("")

	// Instead we can define the size of the underlying array of the slice
	// and keep the size of the slice itself as 0. Then append will start
	// from the beginning of the "reserved space".
	y := make([]int, 0, 5)
	fmt.Print("y's capacity: ")
	for i := 5; i < 10; i++ {
		y = append(y, i)
		fmt.Print(cap(y), " ")
	}
	fmt.Println("\ny (within capacity test):", y)
	fmt.Println("")

	// Now we will see the slice's capacity increase (i.e. it will need to
	// resize itself but copying over the entire array). The strategy seems
	// to be doubling the array capacity each time we exhaust the current
	// capacity.
	fmt.Print("y's capacity: ")
	for i := 10; i < 17; i++ {
		y = append(y, i)
		fmt.Print(cap(y), " ")
	}
	fmt.Println("\ny (growing capacity test):", y)
	fmt.Println("")

	// We can also append multiple elements in a single append call.
	var z []int
	z = append(z, 1, 2, 3)
	fmt.Println("z:", z)
	fmt.Println("")

	// Create slices with a syntax similar to Python's splicing syntax.
	// Though unlike in Python we can't use negative indicies.
	arr := [5]int{1, 2, 3, 4, 5}
	sli1 := arr[0:3] // Excluding index 3 so [1, 2, 3]
	fmt.Println("sli1:", sli1)
	fmt.Println("")

	sli2 := make([]int, 0, 10)
	// If the underlying capacity of sli2 is not greater than or equal to that
	// of sli1 the copy will fail since copy will look at the capacities before
	// copying to ensure stuff like bounds safety.
	// Use regular asssignment like sli2 := sli1 if you want to avoid this issue
	// sli1 will be copied into sli2 (&sli1 != &sli2).
	copy(sli2, sli1)
	sli2 = append(sli2, 1)
	fmt.Println("sli2:", sli2)
	var sli3 []int
	sli3 = append(sli2, 3)
	fmt.Println("sli3:", sli3)
	fmt.Println("sli2:", sli2) // sli2 will be unaffected.
	fmt.Println("")
}

func demoMaps() {
	x := make(map[string]int)
	// If we used var x map[string]int instead then we'd get a runtime error
	// because the map is empty (assignment to entry in nil map)
	x["key"] = 1
	x["entry"] = 2
	fmt.Println("x[\"key\"]:", x["key"])
	fmt.Println("x:", x)
	delete(x, "key")
	fmt.Println("x:", x)

	// The best way to see if the value for a key exists:
	value, exists := x["key"]
	if exists {
		fmt.Println(value)
	} else {
		fmt.Println("no value for the key \"key\"")
	}

	value, exists = x["entry"]
	if exists {
		fmt.Println(value)
	} else {
		fmt.Println("no value for the key \"entry\"")
	}

	// The more idiomatic way of doing this is:
	if val, doesExist := x["something"]; doesExist {
		fmt.Println(val)
	} else {
		fmt.Println("no value for the key \"something\"")
	}

	// Example of how to define a static nested map:
	elements := map[string]map[string]string{
		"H": map[string]string{
			"name":  "Hydrogen",
			"state": "gas",
		},
		"He": map[string]string{
			"name":  "Helium",
			"state": "gas",
		},
		"Li": map[string]string{
			"name":  "Lithium",
			"state": "solid",
		},
		"Be": map[string]string{
			"name":  "Beryllium",
			"state": "solid",
		},
		"B": map[string]string{
			"name":  "Boron",
			"state": "solid",
		},
		"C": map[string]string{
			"name":  "Carbon",
			"state": "solid",
		},
	}
	if element, exists := elements["C"]; exists {
		fmt.Println(element["name"]) // We assume that each element will have the name and state.
	}
}

// There are many a time when I need to sort a slice of slice based on
// some particular element of the inner slice. In my experience, I've
// seen that both Python and C++ have sort methods that support this
// well. Go has something like this too with the sort.Slice function.
func customSortDemo() {
	data := [][]int{
		[]int{1, 2},
		[]int{3, 5},
		[]int{2, 2}
	}
	sort.Slice(data, func(i int, j int) bool {
		return data[i][0] < data[j][0]
	})
	fmt.Println(data)
}

func main() {
	demoMaps()
}
