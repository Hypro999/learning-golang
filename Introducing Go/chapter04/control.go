package main

import "fmt"

// Demo of if, else and switch.
func wordForDigit(digit int) string {
	var word string

	if digit < 0 {
		word = "-"
	} else {
		word = ""
	}

	switch digit {
	case 0:
		word += "zero"
	case 1:
		word += "one"
	case 2:
		word += "two"
	case 3:
		word += "three"
	case 4:
		word += "four"
	case 5:
		word += "five"
	case 6:
		word += "six"
	case 7:
		word += "seven"
	case 8:
		word += "eight"
	case 9:
		word += "nine"
	default:
		word = ""
	}

	return word
}

// Demonstation of the 4 varients of loops.
func loopConstruct() {
	// The first way - infinite loop.
	for {
		break
	}

	// The second way of writing a loop - The while loop.
	i := 0
	for i < 10 {
		fmt.Print(i, " ")
		i++
	}
	fmt.Print("\n")

	// The third way of writing a loop - The original for loop.
	for j := 0; j < 10; j++ {
		fmt.Print(wordForDigit(j), " ")
	}
	fmt.Print("\n")

	// The fourth way of writing a for loop is the "for each" loop.
	// Using the range keyword, we can iterate thorugh an iterable.
	// Valid iterables include: strings, arrays, slices, maps, amd channels.
	// Use an _ in place of index if we don't need the index (similar to
	// the convention in python but here it's a requirement if we won't
	// use the index variable since Go won't let us define unused variables).
	arr := [5]int{
		1,
		2,
		3,
		4,
		5, // When defining an array over multiple lines, this last comma is mandatory (enforced style).
	}
	for index, value := range arr {
		fmt.Print(fmt.Sprintf("(%d, %d) ", index, value))
	}
	fmt.Println("")
}

var activeFoo func() = loopConstruct

func main() {
	activeFoo()
}
