package main

type howDifferentCanItBe struct {
	x int
	y string
} /* At this point I haven't learnt about Go structs but come on, how different can they be from C? */

func clearStruct(a *howDifferentCanItBe) {
	/* Notice that we won't use "->"" but rather just ".".
	 * So in a way the formal parameter "a *howDifferentCanItBe" (or "howDifferentCanItBe *a" from a C perspective)
	 * is more like "howDifferentCanItBe& a" from C++ (maybe not the same under the hood, but they feel similar). */
	a.x = 0
	a.y = ""
}

func setStruct(b *howDifferentCanItBe, x int, y string) {
	b.x = x
	b.y = y
}

func pseudoClearStruct(a howDifferentCanItBe) {
	a.x = 0
	a.y = ""
}

func simplePointersDemo() {
	/* Pointers in Go are very similar to pointers in C. One of the biggest differences though is that
	 * C supports pointer arithmetic whereas Go does not. */
	b := howDifferentCanItBe{10, "Hello"}
	println(b.x, b.y)

	pseudoClearStruct(b)
	println(b.x, b.y) // No change because of pass-by-value

	clearStruct(&b)
	println(b.x, b.y) // Change because of pass-by-reference

	var c *howDifferentCanItBe = new(howDifferentCanItBe) // Just reserve the memory and return a pointer to it.
	setStruct(c, 5, "hi")
	println(c.x, c.y)
	/* Go's built in garbage collector will handle freeing the memory allocated to "var c"
	 * so unlike with C's malloc/calloc we don't need to worry about freeing heap memory
	 * using something like free(). */
}

func main() {
	simplePointersDemo()
}
