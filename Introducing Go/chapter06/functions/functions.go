package main

/* NOTE: receiver functions will be covered seperately after covering structs. */

import "fmt"

func summation(nums ...int) int {
	/* A demonstration of a variadic funtion which will return the sum of
	 * the integers passed to it. */
	sum := 0
	for _, num := range nums {
		sum += num
	}
	return sum
}

func nestedFunc() {
	var innerFooOne func()
	innerFooOne = func() {
		fmt.Println("innerFooOne")
	}

	innerFooTwo := func() {
		fmt.Println("innerFooTwo")
	}

	innerFooOne()
	innerFooTwo()
}

func simpleDemo() {
	li := []int{1, 2, 3, 4, 5}
	sum1 := summation(1, 2, 3, 4, 5)
	fmt.Println(li, sum1)

}

func advancedDemo() {
	/* Demo two things:
	 * 1. the *args calling style for variadic functions.
	 * 2. Closures and how inner functions can modify variables from the surrounding scope. */
	li := []int{1, 2, 3, 4, 5}
	sum1 := summation(1, 2, 3, 4, 5)
	fmt.Println(li, sum1)
	foo := func() {
		li = append(li, 5)
		sum2 := summation(li...)
		// This version of calling a function is something similar to
		// foo(*args) in Python to expand a dict into calling arguments.
		fmt.Println(li, sum2)
	}
	foo()
	fmt.Println(li)
	// A nested function + the variable that are not immediately local to it
	// (but rather come from the surrounding scope) are collectively called
	// a "closure".
}

func makeEvenGenerator() func() uint {
	i := uint(0)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}

func generatorDemo() {
	/* A really cool demostration of using closures to emulate static variables on
	 * a per function reference basis instead of on a global basis. */
	nextEven := makeEvenGenerator()
	fmt.Println(nextEven(), nextEven(), nextEven())
	nextEvenNum := makeEvenGenerator()
	fmt.Println(nextEvenNum(), nextEvenNum(), nextEvenNum())
}

func deferDemo() {
	/* Function calls prefixed with the defer keyword will be called at the very end of enclosing function.
	 * Generally it's used to defer the freeing of resources.
	 * They can be especially useful when there are multiple return statements.
	 * They're also useful since they will be executed even if the calling function enters "panic mode" (an
	 * exception state). */
	tempfoo := func(i int) {
		fmt.Println(fmt.Sprintf("tempfoo %d", i))
	}
	defer tempfoo(1)
	tempfoo(2)
}

func panicAndRecoverDemo() {
	/* A "runtime panic" is basically a hard runtime error - you have to perform some clean up, maybe do some logging and exit.
	 * Runtime exceptions in Go are similar to "return -1 and set errno" in C. Runtime exceptions are meant to be recoverable.
	 * To clean up after a runtime panic, we need to use deferred functions. */
	defer func() {
		fmt.Println("Consider the runtime panic handled.")
	}() // Anonymous inner functions just like in NodeJS
	simpleDemo() // Just to demonstrate that this would be called before the defered function.
	panic("AAAAAAAAHHHHHHHHHH!!!!!!")
}

func main() {
	demos := []func(){simpleDemo, advancedDemo, generatorDemo, deferDemo, panicAndRecoverDemo} // A slice of function references
	demos[4]()
}
