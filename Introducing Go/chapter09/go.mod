// As long as the name is not something that collides with one of the standard library packages
// we can specify a module name that does not contain a domain name extension (dot notation).
module unittests

go 1.14
