package main

import (
	"flag"
	"fmt"
	"os"

	"unittests/validation"
)

func main() {
	portPtr := flag.Int("port", 8080, "")
	flag.Parse()
	port, err := validation.ValidatePort(*portPtr)
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
	fmt.Printf("%d is a valid port number.\n", port)
}
