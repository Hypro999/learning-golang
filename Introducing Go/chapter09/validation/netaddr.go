package validation

import (
	"errors"
	"fmt"
)

const maxPortNumber = 65535

// ValidatePort will validate the input port (int) and if it is valid then it
// will return a uint16 value for the port and no error otherwise 0 and an error.
func ValidatePort(port int) (uint16, error) {
	if port < 0 {
		return uint16(0), fmt.Errorf("Invalid port: %d is a negative number", port)
	}

	if port == 0 {
		return uint16(0), errors.New("0 is actually not a valid port number.")
	}

	if port > maxPortNumber {
		return uint16(0), fmt.Errorf("Invalid port: %d > %d", port, maxPortNumber)
	}

	return uint16(port), nil
}
