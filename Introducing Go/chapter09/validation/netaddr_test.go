package validation

import ogtest "testing"

func TestValidatePort(t *ogtest.T) {
	ports := []int{65535, 65536, 0, -1, 100, 80, 8080, 9001, 90001}
	valid := []bool{true, false, false, false, true, true, true, true, false}

	for i, p := range ports {
		port, err := ValidatePort(p)
		if valid[i] && err != nil {
			t.Errorf("%d is a valid port number. ValidatePort should not return an error.\n", port)
			if port != uint16(p) {
				t.Errorf("want: %d, got: %d\n", uint16(p), port)
			}
		} else if !valid[i] && err == nil {
			t.Errorf("%d is not a valid port number. ValidatePort should return an error.\n", port)
		}
	}
}
