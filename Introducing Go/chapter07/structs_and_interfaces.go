package main

import (
	"errors"
	"fmt"
)

// In go, interfaces only declare an expected behaviour
// In other languages like Java they also declare the
// expected data members.
// Any function which implements these functions is
// automatically associated with this interface.
type player interface {
	GetName() string
	GetAge() uint8
	GetLevel() uint8
	GetLeague() string
}

type member struct {
	name  string
	age   uint8
	level uint8
}

func validatePlayerDetails(name string, age uint8, level uint8) error {
	if name == "" {
		return errors.New("empty name")
	} else if age < 10 {
		return errors.New("invalid age")
	}
	return nil
}

func newPlayer(name string, age uint8, level uint8) (member, error) {
	err := validatePlayerDetails(name, age, level)
	if err == nil {
		return member{name: name, age: age, level: level}, nil
	}
	return member{}, err
}

// An example of a receiver specified for a function.
func (p *member) GetName() string {
	return p.name
}

func (p *member) GetAge() uint8 {
	return p.age
}

func (p *member) GetLevel() uint8 {
	return p.level
}

func (p *member) GetLeague() string {
	if p.level < 50 {
		return "stone"
	} else if p.level < 150 {
		return "iron"
	} else if p.level < 200 {
		return "gold"
	} else if p.level < 250 {
		return "diamond"
	} else {
		return "netherite"
	}
}

func (p *member) String() string {
	return fmt.Sprintf("%s (%s)", p.name, p.GetLeague())
}

type administrator struct {
	member // An embedded type/struct and all of the fields from here are anonymous types.
	key    string
}

func newAdministrator(name string, age uint8, level uint8) (administrator, error) {
	err := validatePlayerDetails(name, age, level)
	if err == nil {
		return administrator{member: member{name: name, age: age, level: level}, key: ""}, nil
	}
	return administrator{}, nil
}

func (a *administrator) String() string {
	return fmt.Sprintf("Admin %s (%s)", a.name, a.GetLeague())
}

func main() {
	steve, err := newAdministrator("Steve", 21, 255)
	if err != nil {
		fmt.Println(err)
	} else {
		// Since the member struct is not an "exported type" the
		// String method won't automatically be called here.
		fmt.Println(steve.String())
	}

	sapnap, err := newPlayer("Sapicus Napicus", 20, 170)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(sapnap.String())
	}

	bbh, err := newPlayer("Muffin Head", 7, 180)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(bbh.String())
	}
}
