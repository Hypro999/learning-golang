package main

import (
	"fmt"
	"unsafe"
)

func strtest() {
	var str string
	str = "Hi"
	fmt.Println(len(str), str)
	fmt.Println(str + " there!")
}

func inttest() {
	/* This is honestly the best we can do in Go. */
	var x int
	x = 999999999999999999
	fmt.Println(unsafe.Sizeof(x))

	var y int8
	y = 127
	fmt.Println(unsafe.Sizeof(y))

	var z int32
	z = 2147483647
	fmt.Println(unsafe.Sizeof(z))

	/* So int by default seems to be 64 bits long on Linux. This means that it's 8 bytes which is like a long int in C. */
}
