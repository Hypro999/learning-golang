package main

import "fmt"

const (
	msg1 = "Hello there!"
	msg2 = "Bulk definition of constants"
)

func main() {
	fmt.Println(msg1)
	fmt.Println(msg2)
}
