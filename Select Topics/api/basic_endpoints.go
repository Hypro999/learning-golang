package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	_ "github.com/go-sql-driver/mysql"

	"github.com/gorilla/mux"
)

const (
	ADDR = ":8000"
)

var (
	db *sql.DB
)

type transaction struct {
	Id  int64
	Src string
	Dst string
}

func extractString(key string, requestData map[string]interface{}, maxLength int) (string, error) {
	maybeStr, ok := requestData[key]
	if !ok {
		err := fmt.Errorf("missing key: %s", key)
		return "", err
	}
	src, ok := maybeStr.(string)
	if !ok {
		err := fmt.Errorf("value for key: %s is not a string", key)
		return "", err
	}
	if len(src) > maxLength {
		err := fmt.Errorf("value for key: %s is too large (maxlength: %d)", key, maxLength)
		return "", err
	}
	return src, nil
}

func ListTransaction(w http.ResponseWriter, r *http.Request) {
	transactions := []transaction{}
	query := `SELECT * FROM transactions`
	rows, err := db.Query(query)
	if err != nil {
		log.Printf("failed to read transactions: %s\n", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer rows.Close()
	for rows.Next() {
		t := transaction{}
		rows.Scan(&t.Id, &t.Src, &t.Dst)
		transactions = append(transactions, t)
	}
	responseBody, err := json.MarshalIndent(transactions, "", "    ")
	if err != nil {
		log.Printf("failed to marshal transactions into JSON")
		w.WriteHeader(500)
		return
	}
	w.Write(responseBody)
}

func RecordTransaction(w http.ResponseWriter, r *http.Request) {
	// First extract the request body as a map.
	requestBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalln(err)
	}
	requestData := map[string]interface{}{}
	err = json.Unmarshal(requestBody, &requestData)
	if err != nil {
		log.Printf("Failed to unmarshal data: %s\n", err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Next extract and validate the arguments.
	src, err := extractString("src", requestData, 30)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	dst, err := extractString("dst", requestData, 30)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	// Finally add the result to the database and return the ID of the transaction.
	query := `INSERT INTO transactions(src, dst) VALUES (?, ?)`
	result, err := db.Exec(query, src, dst)
	if err != nil {
		log.Printf("Failed to insert transaction: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	transactionID, err := result.LastInsertId()
	if err != nil {
		log.Printf("Failed to retrieve transaction ID: %s\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	responseBody, _ := json.MarshalIndent(map[string]int64{
		"transaction_id": transactionID,
	}, "", "    ")
	w.Write(responseBody)
}

func CreateTransactionsTable() {
	query := `
		CREATE TABLE transactions (
			id INT AUTO_INCREMENT PRIMARY KEY,
			src VARCHAR(30),
			dst VARCHAR(30)
		)
	`
	_, err := db.Exec(query)
	if err != nil {
		log.Fatalf("failed to create transactions table: %s\n", err.Error())
	}
	log.Println("created transactions table successfully")
}

func CreateTransactionsTableIfNotExists() {
	query := `
	SELECT TABLE_NAME
	FROM information_schema.tables
	WHERE table_name = 'transactions'
	LIMIT 1;
	`
	rows, err := db.Query(query)
	if err != nil {
		log.Println("created transactions table successfully")
		log.Fatalf("failed to query: %s\n", err.Error())
	}
	defer rows.Close()
	if rows.Next() {
		tableName := ""
		rows.Scan(&tableName)
		if tableName == "transactions" {
			log.Println("transactions table found")
			return
		}
	}
	CreateTransactionsTable()
}

func EstablishDBConnection() {
	var err error
	db, err = sql.Open("mysql", "root:password123@(127.0.0.1:3306)/test?parseTime=true")
	if err != nil {
		log.Fatalf("mysql open: %s\n", err.Error())
	}
	err = db.Ping()
	if err != nil {
		log.Fatalf("mysql ping: %s\n", err.Error())
	}
	log.Println("connection opened successfully")
}

func StartServer() {
	router := mux.NewRouter()
	router.HandleFunc("/transaction/list", ListTransaction).Methods("GET")
	router.HandleFunc("/transaction/record", RecordTransaction).Methods("POST")
	srv := &http.Server{
		Handler:      router,
		Addr:         ADDR,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Printf("listening on %s\n", ADDR)
	log.Fatal(srv.ListenAndServe())
}

func main() {
	EstablishDBConnection()
	CreateTransactionsTableIfNotExists()
	StartServer()
}
