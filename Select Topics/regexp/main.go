package main

import (
	"fmt"
	"regexp"
)

// Luckily Go uses the same syntax as python for regex :)

//  The regexp implementation provided by this package is guaranteed to run in time linear in the size of the input. (This is a property not guaranteed by most open source implementations of regular expressions.) For more information about this property, see: https://swtch.com/~rsc/regexp/regexp1.html

//  There are 16 methods of Regexp that match a regular expression and identify the matched text. Their names are matched by this regular expression: Find(All)?(String)?(Submatch)?(Index)?

const patternString = "^(?P<org>[A-Za-z]{1}[A-Za-z0-9]{2,})/(?P<repo>[A-Za-z0-9]+)$"

func validRepository(name string) bool {
	// Sample pattern of org/repo where org must start with a letter and have at least 3 characters
	// and at most 128 characters. Similarly, repo must also start with a letter but can have anywhere
	// between 1 to 128 characters.
	pattern := regexp.MustCompile(patternString)
	// Compile would return an err variable too, so we use MustCompile which would panic instead.
	return pattern.MatchString(name)
}

func extractValidRepository(name string) map[string]string {
	pattern := regexp.MustCompile(patternString)
	matches := pattern.FindStringSubmatch(name)
	namedGroups := pattern.SubexpNames()
	matchesMap := make(map[string]string)
	for i, value := range matches {
		matchesMap[namedGroups[i]] = value
	}
	return matchesMap
}

func main() {
	names := []string{"golang/go", "123golang/go", "-golang/go"}

	for _, name := range names {
		fmt.Printf("%s: %v\n", name, validRepository(name))
	}

	fmt.Println("")

	for _, name := range names {
		matchesMap := extractValidRepository(name)
		if len(matchesMap) == 0 {
			fmt.Println("No matches found.")
		} else {
			fmt.Printf("%#v\n", matchesMap)
		}
	}
}
